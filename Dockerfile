#docker build -t travel_voyage .
#docker run -dit --restart unless-stopped -p 8080:80 --name travel_voyage travel_voyage 
#docker exec -i -t travel_voyage /bin/bash
#docker rm -f $(echo $(docker ps -a | grep travel_voyage | awk '{print $1}'))

FROM debian:stable

USER root
RUN apt-get update
RUN apt-get upgrade -y

RUN apt-get -y install apache2 mariadb-server php php-mysql
RUN rm /var/www/html/index.html
COPY src/. /var/www/html/
RUN sed -i -e '27,42d' /var/www/html/index.php
RUN service mysql start && sleep 10 && mysql < /var/www/html/create.sql &&  mysql --execute="GRANT ALL PRIVILEGES ON *.* TO 'projet'@'localhost' IDENTIFIED BY 'tejorp';" && service mysql stop && rm /var/www/html/create.sql

EXPOSE 80

CMD ["/bin/bash", "-c", "service mysql start && /usr/sbin/apache2ctl -DFOREGROUND"]
