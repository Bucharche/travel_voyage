<?php


    function echoRecherche(){
        echo("<nav>
                <ul>
                    <li>
                        <a href=\"?r=rapide\" ".isCurrent("rapide"). " >
                            Rapide
                        </a>
                    </li>
                    <li>
                        <a href=\"?r=avancee\" ".isCurrent("avancee"). " >
                            Avancée
                        </a>
                    </li>
                    <li>
                        <a href=\"?r=guidezmoi\" ".isCurrent("guidezmoi"). " >
                            Guidez-moi
                        </a>
                    </li>
                </ul>
            </nav>");
		if(isset($_GET['r'])){ // 'R' pour type de Recherche (parmi les 3)
			switch($_GET['r']) {
				case "avancee" :
					include("recherche/avancee.php");
					break;
				case "guidezmoi" :
					include("recherche/guidez-moi.php");
					break;
				default :
					include("recherche/rapide.php");
			}
		}else{
			include("recherche/rapide.php");
		}
    }
	
	function isCurrent($quoi){
		if(isset($_GET["e"])){
			if($_GET["e"]==$quoi){
				echo("id=\"currentEtape\"");
			}
		}elseif($quoi=="recherchez"){
			echo("id=\"currentEtape\"");
		}
		if(isset($_GET["r"])){
			if($_GET["r"]==$quoi){
				return("id=\"currentRecherche\"");
			}
		}elseif($quoi=="rapide"){
			return("id=\"currentRecherche\"");
		}
	}
?>

<h2>Organisez vos vacances en 3 étapes</h2>
<!-- Made by juju à modifier ou corriger selon votre bon plaisir PS : attention à modifier le css en fonction de la taille de ce paragraphe -->
<p>Vous souhaitez organiser vos vacances facilement et rapidement ? Travel Voyage est là pour vous !</p>
<p>Suivez nos trois étapes : recherche, sélection et plannification. Travel Voyage réserve vos vacances !<br />
Vous pouvez rechercher un hébergement, des restaurants, des loisirs ou un moyen de transport dans votre région.</p>
<p>Consultez également nos packs !</p>
<p>Bonne visite =)</p>

<nav id="etapes">
    <ul>
        <li>
            <a href="?e=recherchez" <?php isCurrent("recherchez"); ?> >
                1- Recherchez
            </a>
        </li>
        <li>
            <a href="?e=selectionnez" <?php isCurrent("selectionnez"); ?> >
                2- Sélectionnez
            </a>
        </li>
        <li>
            <a href="?e=planifiez" <?php isCurrent("planifiez"); ?> >
                3- Planifiez
            </a>
        </li>
    </ul>
</nav>

<?php 
	if(isset($_GET['e'])){  // 'E' pour Etape (parmi les 3)
	echo("<section id=\"".htmlentities($_GET['e'])."\">");
		switch($_GET['e']) {
			case "selectionnez" :
				include("recherche/resultatRecherche.php");
				break;
			case "planifiez" :
				include("planning/nico.php");
				break;
			default :
				echoRecherche();
		}
	}else{
		echo("<section id=\"recherchez\">");
		echoRecherche();
	}
	echo("</section>");
?>

<section id="bonplans">
	<?php
		include("annonce/bonplans.php");
	?>
</section>