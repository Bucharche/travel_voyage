<?php
	$types = array(
		"Activités" => "activite",
		"Restauration" => "restauration"
	);
	
	$activite = array(
		"Culturelles" => "culturelle",
		"Sportives" => "sportive",
		"Loisirs" => "loisir",
		"Autres" => "autre"
	);
	
	$restauration = array(
		"Bars" => "bar",
		"Restaurants" => "restaurant",
		"Snacks" => "snack",
		"Autres" => "autre"
	);
	
	if(isset($_GET['t']) // 'T' pour Type
	&& isset($_GET['s']) // 'S' pour Soustype 
	&& in_array($_GET['t'], $types)
	&& (in_array($_GET['s'], $activite) || in_array($_GET['s'], $restauration))
	){
		
		// Gros bloc : Si on a reçu une catégorie particulière à afficher :
		
		try{
			$bdd = new PDO('mysql:host=localhost;dbname=projet;charset=utf8', 'projet', 'tejorp');
		}catch(Exception $e){ die('Erreur : '.$e->getMessage()); }
		
		$reponse = $bdd->query("select * from `annonces` where typeservice='".$_GET['t']."' and soustype='".$_GET['s']."' order by nom");
		
		if($reponse && $reponse->rowCount()>0){ //Si la requête a renvoyé quelque chose
			$nbParPage = isset($_GET["nbParPage"]) ? $_GET["nbParPage"] : 10;
			$index = isset($_GET["index"]) ? $_GET["index"] : 0;
			
			echo("<nav>Page : ");
			for($nbPage=0; $nbPage*$nbParPage<$reponse->rowCount(); $nbPage++){
				echo("<a href=\"?q=activites&amp;t=".$_GET['t']."&amp;s=".$_GET['s']."&amp;index=".($nbPage*$nbParPage)."&amp;nbParPage=$nbParPage\">".($nbPage+1)." </a>");
			}
			?>
			<form action="?" method="GET">
				<label for="nbParPage">Nombre par page : </label>
				<input id="nbParPage" name="nbParPage" type="number" min="1" value="<?php echo($nbParPage); ?>" />
				<input id="index" style="display : none;" name="index" type="text" value="0" />
				<input id="q" style="display : none;" name="q" type="text" value="<?php echo($_GET['q']); ?>" />
				<input id="t" style="display : none;" name="t" type="text" value="<?php echo($_GET['t']); ?>" />
				<input id="s" style="display : none;" name="s" type="text" value="<?php echo($_GET['s']); ?>" />
				<input type="submit" id="ok" value="Appliquer" />
			</form>
			<?php // Ci-dessus, les champs q t s et index sont l) pour avoir les bons paramètres dans le GET
			
			echo("</nav>");
			
			for($init=0; $init<$index; $init++){ // On avance jusqu'à l'index de départ souhaité
				$temp = $reponse->fetch();
			}
			
			while($donnees = $reponse->fetch()){
				if($nbParPage>0){
					echo("<a href=\""."?q=annonce&amp;id=".$donnees['id']."\">\n");
					echo("\t<h2>".$donnees['nom']."</h2>\n");
					echo("\t<p>".$donnees['description']."</p>\n");
					echo("</a>\n");
				}
				$nbParPage--;
			}
			
		}else{
			echo("<p>Pas d'activités pour cette catégorie. <a href=\"?q=activites\">Retour</a> </p>");
		}
		
	}else{
		
		// Sinon : on affiche toutes les catégories en lien cliquable
	
		foreach($types as $Taffiche => $type){
			echo("<section>\n<h2>$Taffiche</h2>\n");
			foreach($$type as $Saffiche => $soustype){
				echo("<a href=\"?q=activites&amp;t=$type&amp;s=$soustype\">$Saffiche</a>\n");
			}
			echo("</section>");
		}
		
	}
?>