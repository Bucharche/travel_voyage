<script>
function setStar(what, howmuch){
	document.getElementById(""+what).value = howmuch;
	elements = document.getElementsByClassName("star "+what);
	for (var i = 0; i < elements.length; i++) {
			elements[i].style.color = i < howmuch ? "#FF0" : "black";
			elements[i].style.textShadow = i < howmuch ? "0px 0px 5px #000" : "0px 0px 5px #FFF";
	}
}	
</script>

<?php
		
		require_once("compte/util.php");
		require_once("img/util.php");
		require_once("util.php");
		function echoStars($nomId){
				for($i=1; $i<6; $i++){
						echo("<span class=\"star $nomId\" onclick=\"setStar('$nomId',$i)\" >&#9733;</span>");
				}
		}
		function getStars($nb){
				$r = "";
				for($i=0; $i<5; $i++){
						$r .= ($i <= $nb-0.5 ? "<span class=\"yellowStar\">&#9733;</span>" : "<span class=\"blackStar\">&#9733;</span>");
				}
				return $r;
		}
				
		try{
			$bdd = new PDO('mysql:host=localhost;dbname=projet;charset=utf8', 'projet', 'tejorp');
			$bdd -> exec("set names utf8");
		}catch(Exception $e){
			die('Erreur : '.$e->getMessage());
		}
		
        if (isset($_GET['id'])){
            $annonces=$_GET['id'];
        } else {
            $annonces=-1;
        }
		if (isset($_GET['nbParPage']) && $_GET['nbParPage']>=1){
			$nbParPage=$_GET['nbParPage'];
			setcookie('nbParPage',$nbParPage,(time()+ 365*24*3600),'/');	
		} else if (isset($_COOKIE['nbParPage']) && $_COOKIE['nbParPage']>=1) {
			$nbParPage=$_COOKIE['nbParPage'];
		} else {
			$nbParPage=10;
		}
		if (!isset($_GET["index"]) || $_GET["index"]<0){
			$index=0;
		} else {
			$index=$_GET["index"];
		} 
		$estConnecte=estConnecte();
		if ($estConnecte==1 || $estConnecte==2){
			$session=$_COOKIE['id_util'];
		} else {
			$session=NULL;
		}
		if (isset($_POST['commentaire'])) {$commentaire=$_POST['commentaire'];}
		if (isset($_POST['note'])){$note=$_POST['note'];}
		
		foreach ($_POST as $key => $value){
			$tmp=explode("_",$key);
			if ($tmp[0]=="sup" && isset($tmp[1])){ //On verifie l'auteur pour confirmer la supression
				$sql=$bdd->query("SELECT idCompte FROM photos WHERE id=$tmp[1]")->fetch();
				if ($sql['idCompte']==$session){
					deleteImage($tmp[1]);
				}
			}			
		}		
		
		$pan=panier($bdd,$annonces);	

		upload($session,$annonces);		
		  
		 //,c.nom as nomCompte,c.prenom,c.email,p.societe,p.tel 
        $reponse = $bdd->query("SELECT c.id as idCompte,c.pseudo,a.id as idAnnonce,a.nom as nomAnnonce,a.description,a.materiel
								FROM annonces a,comptes c, prestataires p where a.id=$annonces and a.idPrestataires=c.id and c.id=p.id");
        $donnees = $reponse->fetch();
        if ($donnees==null){
            echo("<p>Annonce introuvable</p>\n");
        } else {
            $imagePrestataire = $bdd->query("SELECT id,idCompte,extension from `photos` where idAnnonce=$annonces AND idCompte=$donnees[idCompte]");
            $imageVacancier = $bdd->query("SELECT id,idCompte,extension from `photos` where idAnnonce=$annonces AND NOT idCompte=$donnees[idCompte]");
            
			echo "<div id=\"desc\">\n";
            echo("<h2><span>Annonce </span>".$donnees['nomAnnonce']." en détail</h2>\n");
			$prix=$bdd->query("SELECT prix FROM V_prixMin WHERE idAnnonce=$donnees[idAnnonce]")->fetch();
			if ($prix['prix']==null){
				echo "<p>Attention, il n'y pas de séance pour cette annonce.</p>";
			} else {
				echo "<p>A partir de $prix[prix] €</p>";
			}
            echo("<p><span>Description : </span>".$donnees['description']."</p>\n");
            
			if ($requete=$bdd->query("SELECT nbEtoiles,nbPlaces,lieu FROM hebergements WHERE id=$donnees[idAnnonce]")->fetch()){
				echo("<p><span>Place : </span>$requete[nbPlaces]</p>\n");
				if (isset($requete['nbEtoiles'])){
					echo("<p><span>Nombre d'etoiles : </span>$requete[nbEtoiles]</p>\n");
				}
				echo("<p><span>Lieu : </span>$requete[lieu]</p>\n");
			} elseif ($requete=$bdd->query("SELECT nbEtoiles FROM restaurations WHERE id=$donnees[idAnnonce]")->fetch()){
				if (isset($requete['nbEtoiles'])){
					echo("<p><span>Nombre d'etoiles : </span>$requete[nbEtoiles]</p>\n");
				}
			} elseif ($requete=$bdd->query("SELECT niveau FROM sports WHERE id=$donnees[idAnnonce]")->fetch()) {
				if (isset($requete['niveau'])){
					echo("<p><span>Niveau : </span>".$requete['niveau']."</p>\n");
				}
			}
			
			echo("<p><span>Matériel Requis : </span>");
            if ($donnees['materiel']==null){
                echo("Aucun</p>\n");
            } else {
                echo($donnees['materiel']."</p>\n");
            }
            echo("<p><span>Proposée par : </span><a href=\"?q=prestataire&id=$donnees[idCompte]\">$donnees[pseudo]</a></p>\n");
//			echo "</div>\n<div id=\"panier2\">\n";
            
			if ($estConnecte==1 || $estConnecte==0){
				$chaine="?";
				foreach($_GET as $key => $value){
					$chaine.="$key=$value&";
				}
				$chaine=substr($chaine,0,-1);
				echo "<form  method=\"post\" action=\"$chaine\">\n";
				if ($pan){
					echo "<input type=\"submit\" id=\"panier\" value=\"Retirer du panier\" name=\"panier\"/>";
				} else {			
					echo "<input type=\"submit\" id=\"panier\" value=\"Ajouter au panier\" name=\"panier\"/>";
				}
				echo "\n</form>";
			}
			echo "</div>";
			
			//////////////////////PHOTOOOOOOOOOOOOO
			if ($imagePrestataire->rowCount()!=0){
				echo"<div>\n";
                echo("<h3>Photos du prestataire</h3>\n");
                foreach($imagePrestataire as $value){
                    echo "<a href=\"img/imgAnnonce/reel/$value[id].$value[extension]\"> ";
                    echo("<img src=\"img/imgAnnonce/miniature/$value[id].$value[extension]\" alt=\"Photo\" height=\"200\" /></a>\n");
                }    
				echo"</div>\n";
            }
            if ($imageVacancier->rowCount()!=0){
				echo"<div id=\"photo\">\n";
                echo("<h3>Photos des vacanciers</h3>\n");
                foreach($imageVacancier as $value){
					if ($estConnecte==1 && $session==$value['idCompte']) {
						echo "<form method=\"POST\" action=\"$chaine\">\n";
					}
					echo "<a href=\"img/imgAnnonce/reel/$value[id].$value[extension]\"> ";
                    echo("<img src=\"img/imgAnnonce/miniature/$value[id].$value[extension]\" alt=\"Photo\" height=\"200\" /></a>\n");
					if ($estConnecte==1 && $session==$value['idCompte']) {
						?>
								<input type="submit" name="sup_<?php echo $value['id']; ?>" value="Supprimer" />
							</form>
						<?php
					}
                }    
				echo"</div>\n";
            }   
			
			commentaires($bdd,$annonces);
        }
		
	//retroune, apres operation, 0 si pas dans panier, 1 si dans panier 
	function panier($bdd,$annonces){
		global $estConnecte;
		if (isset($_POST['panier']) && $_POST['panier']=="Retirer du panier"){		//retire du panier -> l'annonce n'est plus dans le panier
			if ($estConnecte==1){
				$bdd->exec("DELETE FROM `paniers` WHERE idCompte=$_COOKIE[id_util] and idAnnonce=$annonces");
			} elseif ($estConnecte==0){
				if (isset($_COOKIE['panier'])) {
					$cook="";
					foreach(explode("/",$_COOKIE['panier']) as $value){
						if ($value!=$annonces){
							$cook.=$value."/";
						}
					}
					$cook=substr($cook,0,-1);
					setcookie('panier',$cook,(time()+ 365*24*3600),'/');
				}
			}
			return 0; 
		} else if (isset($_POST['panier']) && $_POST['panier']=="Ajouter au panier"){	//ajoute au panier -> l'annonce est dans le panier		
			if ($estConnecte==1){
				$bdd->exec("INSERT INTO `paniers`(`idCompte`, `idAnnonce`) VALUES ('$_COOKIE[id_util]','$annonces')");
			} elseif ($estConnecte==0){
				if (isset($_COOKIE['panier']) && !strstr($_COOKIE['panier'],$annonces)){
					setcookie('panier',$_COOKIE['panier']."/$annonces",(time()+ 365*24*3600),'/');
				}else if (!isset($_COOKIE['panier'])){
					setcookie('panier',"$annonces",(time()+ 365*24*3600),'/');
				}
			}
			return 1;
		} else if (($estConnecte==0 && isset($_COOKIE['panier']) && strstr($_COOKIE['panier'],$annonces)) || ($estConnecte==1 && $bdd->query("SELECT * FROM paniers WHERE idCompte=$_COOKIE[id_util] and idAnnonce=$annonces")->fetch())) {
			// ne fait rien mais l'annonce est dans le panier
			return 1;
		} else { // ne fait rien mais l'annonce n'est pas dans le panier
			return 0;
		}
	}
	
	function commentaires($bdd){
		global $nbParPage;
		global $index;
		global $estConnecte;
		global $commentaire;
		global $note;
		global $session;
		global $annonces;
		global $tailleMaxImage;
		
		foreach($_POST as $key =>$value){
			$explose=explode('_',$key);
			if($explose[0]=='sup' && isset($explose[1])){
				$propriaitaire=$bdd->query("SELECT idCompte FROM commentaires WHERE id=$explose[1]")->fetch();
				if ($propriaitaire['idCompte']==$session){
					$bdd->exec("DELETE FROM commentaires WHERE id=$explose[1]");
				}
			}
		}
		
		if (isset($commentaire) && isset($note) && $note>=0 and $note<=5 && $estConnecte==1){
			$r = $bdd->query('SELECT max(id) FROM `commentaires`')->fetch();
			$idCom=$r['max(id)']+1;
			$insert="INSERT INTO `commentaires`(`id`, `idCompte`, `idAnnonce`, `note`, `description`) VALUES ($idCom,$session,$annonces,$note,".$bdd->quote($commentaire).")";
			$bdd->exec($insert);
		}
		
		$chaine='?';
		foreach ($_GET as $key => $value) {
			$chaine.="$key=$value&";
		}
		$chaine=substr($chaine,0,-1);
		
		echo "<div id=\"commentaire\">";
		echo "<h3>Commentaires</h3>";
		$reponse = $bdd->query("select avg(note) as moyenne from commentaires where idAnnonce=$annonces");
		$donnee=$reponse->fetch();		
		if($donnee['moyenne']!=null){
			echo "<p><span>Moyenne : </span>".getStars($donnee['moyenne'])."</p>";
		}
		
		$reponse = $bdd->query("select pseudo,note,description,idCompte,ca.id from commentaires ca,comptes c where idAnnonce=$annonces and idCompte=c.id order by ca.id");
		if($reponse && $reponse->rowCount()>0){ //Si la requête a renvoyé quelque chose
			
			echo("<nav>Page : ");
			$chaine="?";
			foreach($_GET as $key => $value){
				if($key!="index" && $key!="nbParPage"){
					$chaine.="$key=$value&";
				}
			}
			$chaine=substr($chaine,0,-1);
			if ($index>0){
				echo("<a href=\"$chaine&index=".($index-$nbParPage)."&nbParPage=$nbParPage\"> Precedent </a>");
			}
			for($nbPage=0; $nbPage*$nbParPage<$reponse->rowCount(); $nbPage++){
				echo("<a href=\"$chaine&index=".($nbPage*$nbParPage)."&nbParPage=$nbParPage\">".($nbPage+1)." </a>");
			}
			if ($index<($reponse->rowCount()-$nbParPage)){
				echo("<a href=\"$chaine&index=".($index+$nbParPage)."&nbParPage=$nbParPage\"> Suivante </a>");
			}
			
			echo "<form action=\"?\" method=\"GET\">";
			foreach($_GET as $key => $value){ //Permet de envoyer la page avec les meme valeur en GET
				if ($key!="nbParPage"){
					echo "<input id=\"$key\" style=\"display : none;\" name=\"$key\" type=\"text\" value=$value />\n";
				}
			}
			?>
				<label for="nbParPage">Nombre par page : </label>
				<input id="nbParPage" name="nbParPage" type="number" min="1" value="<?php echo($nbParPage); ?>" />
				<input type="submit" id="ok" value="Appliquer" />
			</form>
			<?php
			
			echo("</nav>");
			
			for($init=0; $init<$index; $init++){ // On avance jusqu'à l'index de départ souhaité
				$temp = $reponse->fetch();
			}
			
			$donnees = $reponse->fetch();
			while($donnees && $nbParPage>0){
				echo "<div>\n";
				echo "<p><span>Pseudo : </span>".$donnees['pseudo']."</p>\n";
				echo "<p><span>Note : </span>".getStars($donnees['note'])."</p>\n";
				echo "<p><span>Commentaire : </span>".$donnees['description']."</p>\n";
				if ($donnees['idCompte']==$session){
					echo "<form method=\"POST\" action=\"$chaine\">\n";
					echo "<input type=\"submit\" value=\"Supprimmer\" name=\"sup_$donnees[id]\" id=\"sup_$donnees[id]\" /> \n";
					echo "</form>\n";
				}
				echo "</div>\n";
				$nbParPage--;
				$donnees = $reponse->fetch();
			}
						
		}else{
			echo("<p>Pas de commentaire pour cette annonce.");
		}
		
		echo "<div>\n";
			
		if ($estConnecte==1){
			echo "<form method=\"POST\" action=\"$chaine\">";
			?>
				<fieldset>
					<legend>Ajouter Commentaire</legend>
					<p>
						<label for="note">Note : *</label>
						<?php echoStars("note"); ?>
						<input type="number" value="" id="note" name="note"  min="1" max="5" style="display : none" required />
					</p>

					<p>  
                    	<label for="commentaire">Commentaire : *</label>
                    	<textarea name="commentaire" id="commentaire" rows="2" cols="50" style="display:inline-block;" placeholder="Votre commentaire (1000 caractères maximum)" maxlength="1000" required></textarea>
						<input type="submit" value="Ajouter le commentaire" /> 
                	</p>
				</fieldset>
			</form>
			<?php
			echo "<div>\n";
		
			echo "</div>\n";
			
			echo "<div>\n";
			echo "<form method=\"POST\" action=\"$chaine\" enctype=\"multipart/form-data\">";
			?>
				<fieldset>
					<legend>Ajouter une Photo</legend>
				<input type="hidden" name="MAX_FILE_SIZE" value="<?php echo $tailleMaxImage; ?>" />
				<input type="file" name="monfichier" />
				<input type="submit" value="Envoyer"/>
				</fieldset>
			<?php
			echo "</form>";
			
		} else if ($estConnecte==2){
			echo "<p>Les prestataires ne peuvent pas mettre des commentaire ou des photos.</p>";
		} else {
			echo "<p>Vous devez être connecté pour poster un commentaire ou une photo.</p>";
		}
		echo "</div>\n";
		echo "</div>\n";
		echo "</div>\n";
	}

?>
