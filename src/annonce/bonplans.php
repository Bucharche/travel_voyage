<h2> Bons plans </h2>

<?php 
	function echoAnnonce($row) {
		echo("<p><span>Nom :</span>".$row["nom"]."</p>");
		echo("<p><span>Description :</span>".$row["description"]."</p>");
		echo("<p><span>Materiel :</span>".$row["materiel"]."</p>");
		echo("<p><span>Type :</span>".$row["typeservice"]."</p>");
		echo("<p><span>Sous-type :</span>".$row["soustype"]."</p>");
	}
	
	function getStars($nb){
			$r = "";
			for($i=0; $i<5; $i++){
					$r .= ($i <= $nb-0.5 ? "<span class=\"yellowStar\">&#9733;</span>" : "<span class=\"blackStar\">&#9733;</span>");
			}
			return $r;
	}
	
	$NB_PROMO = 1; // Nombre de promotion affiché
	$NB_POPU = 1; // Nombre d'annonce populaires affiché
	$NB_RECENT = 1; // Nombre d'annonce récentes affiché
	
	// Connection
    try{
		$bdd = new PDO('mysql:host=localhost;dbname=projet;charset=utf8', 'projet', 'tejorp');
    }catch(Exception $e){
        die('Erreur : '.$e->getMessage());
    }  
	
	// Promotions
	
	echo("<article><h3>Promotions</h3>");
	
	if($NB_PROMO<=1) {
		$NB_PROMO = 1;
		echo("<h4>L'annonce avec la meilleure promotion est : </h4>");
	} else {
		echo("<h4>Les $NB_PROMO annonces les plus populaires sont : </h4>");
	}
	
	$sql = "SELECT *\n"
    . "FROM promotions p, annonces a\n"
    . "WHERE p.idAnnonce = a.id\n"
    . "ORDER BY p.reduc DESC LIMIT $NB_PROMO";

	$data = $bdd->prepare($sql);
	$data->execute();
	
	
	while($row = $data->fetch()) {
		echo("<a href=\"?q=annonce&amp;id=".$row["id"]."\">");
		echo("<p><span>Promotion :</span>".$row["libelle"]."</p>");		
		echo("<p><span>Réduction :</span>".$row["reduc"]."%</p>");
		echoAnnonce($row);
		echo("</a>");
	}
	
	echo("</article>");
	
	// Populaire
	
	echo("<article><h3>Les mieux notées</h3>");
	
	if($NB_POPU<=1) {
		$NB_POPU = 1;
		echo("<h4>L'annonce la mieux notée est : </h4>");
	} else {
		echo("<h4>Les $NB_POPU annonces les mieux notées sont : </h4>");
	}
	
	/*$sql = "SELECT *\n"
    . "FROM annonces a,commentaires c\n"
    . "WHERE c.idAnnonce=a.id\n"
    . "ORDER BY note DESC LIMIT $NB_POPU";
	
	$data = $bdd->prepare($sql);
	$data->execute();
	
	while($row = $data->fetch()) {
		echo("<a href=\"?q=annonce&amp;id=".($row["id"]+1)."\">");
		echoAnnonce($row);
		echo("<p><span>Note :</span>".$row["note"]."</p>");
		echo("</a>");
	}*/
	$reponse=$bdd->query("SELECT a.id, avg(note) as moyenne, a.nom, a.description,a.materiel,a.typeservice,a.soustype FROM commentaires c, annonces a WHERE a.id=c.idAnnonce GROUP BY id ORDER BY avg(note) DESC LIMIT $NB_POPU");
	
	while($sql=$reponse->fetch()){
		echo("<a href=\"?q=annonce&amp;id=".($sql["id"])."\">");
			echoAnnonce($sql);
			echo("<p><span>Note :</span>".getStars($sql["moyenne"])."</p>");
			echo("</a>");
	}
	
	echo("</article>");
	
	// Récent
	
	echo("<article><h3>Les plus récentes</h3>");
	
	if($NB_RECENT<=1) {
		$NB_RECENT = 1;
		echo("<h4>L'annonce la plus récente est : </h4>");
	} else {
		echo("<h4>Les $NB_RECENT annonces les plus récentes sont : </h4>");
	}

	$sql = "SELECT * FROM `annonces` ORDER BY id DESC LIMIT $NB_RECENT";
	$data = $bdd->prepare($sql);
	$data->execute();
	
	while($row = $data->fetch()) {
		echo("<a href=\"?q=annonce&amp;id=".$row["id"]."\">");
		echoAnnonce($row);
		echo("</a>");
	}
	
	echo("</article>");
	
?>


