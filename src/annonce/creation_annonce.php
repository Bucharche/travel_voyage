 <?php 
	

	// on récupère les données du formulaire, on échappe; on sécurise la saisie qui vient de l'user
	$r = $_POST;
	foreach($r as &$v){
		$v = addslashes($v);
		$v = htmlspecialchars($v);
	}	
	unset($v);
	
	// on essaye de se connecter à la BDD
	try
	{
		$bdd = new PDO('mysql:host=localhost;dbname=projet;charset=utf8', 'projet', 'tejorp');
	}
	catch(Exception $e)
	{
			die('Erreur : '.$e->getMessage());
	}
	
	// fonction qui donne un nouvel id d'annonce
	function incrIDAnnonce($co){
		$reponse = $co->query('SELECT max(id) FROM `annonces`');
		$donnees = $reponse->fetch();
		return ($donnees['max(id)']+1);
	}

	$tags = '\'';
	foreach($r as $s){
		$tags .= ' '.$s.' ' ;
	}
	unset($s);
	$tags .= '\'';
	
	$id = incrIDAnnonce($bdd);

	// on crée une annonce qui dépend de ce que l'utilisateur veut : dans tous les cas on insère au moins dans la table `annonce`
	switch ($r['typeservice']) {
		case "activite" :
			
			// pas forcément de matériel
			if ($r['materiel'] == "") $materiel = "";
			else $materiel = $r['materiel'];
			
			// on définit les horaires libres
			if ($r['horairelibre'] == "oui") $horairelibre = "1";
			else $horairelibre = "0";
			
			$req = "INSERT INTO `annonces` VALUES ('$id"
				."' , '$_COOKIE[id_util]' , '$r[intitule]' , '$r[description]'"
				." , '$materiel' , '$r[typeservice]' , '$r[soustype]'"
				." , '$horairelibre', $tags);";
			$bdd ->exec($req);
			
			if($r['soustype'] == "sportive" ) {
				
				$req_sport = "INSERT INTO `sports` VALUES ('$id', '$r[niveau]');";
				
				echo"$req_sport";
				$bdd -> exec($req_sport);
			}
			break;
			
		case "hebergement" :
			// les horaires sont forcément libres
			$horairelibre = "1";
			
			// pas forcément de materiel
			if ($r['materiel'] == "") $materiel = "";
			else $materiel = $r['materiel'];
			$req = "INSERT INTO `annonces` VALUES ('$id"
				."' , '$_COOKIE[id_util]' , '$r[intitule]' , '$r[description]'"
				." , '$materiel' , '$r[typeservice]' , '$r[soustype]'"
				." , '$horairelibre', $tags);";
			$bdd ->exec($req);
			// la requete dans `hébergements`
			$req_h = "INSERT INTO `hebergements` VALUES ('$id', '$r[nb_places]', '$r[nb_etoiles]', '$r[lieu]');";
			$bdd ->exec($req_h);
			
			break;	
		case "restauration" :
		
			$horairelibre = "true";
			$materiel = "";
			$req = "INSERT INTO `annonces` VALUES ('$id"
				."' , '$_COOKIE[id_util]' , '$r[intitule]' , '$r[description]'"
				." , '$materiel' , '$r[typeservice]' , '$r[soustype]'"
				." , $horairelibre, $tags);";
			$bdd ->exec($req);
			
			$req_r = "INSERT INTO `restaurations` VALUES ('$id','$r[nb_etoiles]', '$r[lieu]');";
			$bdd ->exec($req_r);
			break;
		default : echo "<p>Erreur lors de la création de l'annonce. <a href=\"?q=creerA\">Réessayer</a></p>";
	} 

	echo "<p>L'annonce a été créée avec succès ! Vous allez être redirigé vers votre page de compte.</p>";
	echo("<script type=\"text/javascript\">setTimeout(\"location.href = '/projet/commun2/?q=moncompte';\",10);</script>");
	
	?>

