	<?php 
	include "/var/www/clients/client1/web3/web/projet/commun2/compte/util.php";
		$etat = estConnecte();
	if ($etat==0){ // non connecté
		echo "<p>Vous devez être connecté pour accéder à cette page.";
		echo "<a href= \"?q=connexion\" >Se connecter   </a>" ;
		echo "<a href=\"?q=inscription\" > S'inscrire </a>";
	}else if ($etat<0){ // cookie non valide
		echo "<p>Je sais que tu as titillé tes cookies, petit vilain (ou que la base de données a des problèmes...)</p>";
	}else if ($etat==1) { 
		echo "<p>Vous devez posséder un compte prestataire pour accéder à cette page</p>";
	}else if($etat==2){
	?>
        <h2>Création d'annonces</h2>

        <form method="post" action="/projet/commun2/annonce/creation_annonce.php">
            <fieldset >
                <legend style="font-size: large"> Créez votre annonce  </legend>
                <p>
                    <label for="intitule">Intitulé : *</label>
                    <input type="text" name="intitule" id="intitule" size="50" maxlength="50" autofocus required title="Donnez un nom à votre annonce" placeholder="Votre intitulé"/>
                </p>
 
                <p  title="Décrivez l'annonce que vous proposez (1000 caractères maximum)"> 
                    <label for="description">Description : *</label>
                    <textarea name="description" id="description" rows="2" cols="50" style="display:inline-block;" placeholder="Votre description (1000 caractères maximum)" maxlength="1000" required></textarea>
                </p>

                <p title="Sélectionnez le type d'annonce que vous proposez">
                    <label>Type : </label>
                    <select name="typeservice" id="typeservice" >
                        <option value="activite">Activité</option>
                        <option value="hebergement"> Hébergement</option>
                        <option value="restauration">Restauration</option>
                    </select>
                </p> 

                <section title="Précisez la catégorie dans laquelle entre votre annonce" style="display:none;" id="sous_type">
                    
					<fieldset class="disabled" id="gestion_a" style="display:none;" disabled>
						
						<label>Catégorie : </label>
						<select name="soustype" id="soustypeA"  >
							<option value="culturelle">Culturelle</option>
							<option value="loisir">Loisir</option>
							<option value="sportive">Sportive</option>
							<option value="autre_activite">Autre</option>
						</select>

						<p title="Indiquez si du matériel est requis de la part des clients pour participer à votre activité (facultatif)">
							<label for="materiel">Matériel nécessaire : </label>
							<input type="text" name="materiel" id="materiel" size="75" maxlength="60" placeholder="Votre matériel requis (facultatif)"/>
						</p>
						
						<p title="Indiquez si l'activité que vous proposez a des horaires libres (le client peut venir à l'heure qu'il souhaite) ">
							<label >Horaires libres : *</label>
							<input type="radio" name="horairelibre" value="oui" id="libre_oui" checked/> <label for="libre_oui">Oui</label>
							<input type="radio" name="horairelibre" value="non" id="libre_non" /> <label for="libre_non">Non</label>
						</p>
						
						<fieldset class="disabled" id="sport" style="display:none;" disabled>
						<p title="Indiquez le niveau de difficulté de votre activité :  ">
							<label >Difficulté : *</label>
							<input type="radio" name="niveau" value="pourTous" id="niveau_tous" checked/> <label for="niveau_tous">Pour tous</label>
							<input type="radio" name="niveau" value="debutant" id="niveau_deb" /> <label for="niveau_deb">Débutant</label>
							<input type="radio" name="niveau" value="confirme" id="niveau_conf" /> <label for="niveau_conf">Confirmé</label>
							<input type="radio" name="niveau" value="expert" id="niveau_exp" /> <label for="niveau_exp">Expert</label>
						</p>
						</fieldset>
						
					</fieldset>
					
					<fieldset class="disabled" id="gestion_h" style="display:none;" disabled>
						<label>Catégorie : </label>
						<select name="soustype" id="soustype" >
							<option value="auberge">Auberge de jeunesse</option>
							<option value="camping">Camping</option>
							<option value="chambres">Chambres d'hôtes</option>
							<option value="gite">Gîte</option>
							<option value="hotel">Hôtel</option>
							<option value="refuge">Refuge</option>
							<option value="autre_hebergement">Autre</option>
						</select>
						
						<p  title="Donnez l'adresse de votre établissement"> 
							<label for="description">Adresse : *</label>
							<textarea name="lieu" id="lieu" rows="2" cols="50" style="display:inline-block;" placeholder="L'adresse de votre établissement (1000 caractères maximum)" maxlength="1000" required></textarea>
						</p>
						
						<p title="Entrez le nombre de places disponibles dans votre hébergement (minimum : 1, maximum : 50)"> 
							<label>Nombre de places proposées : *</label>
							<input value="1" id="nb_places" name="nb_places"type="number" min="1" max="50" required/>
						</p>
						
						<p title="Entrez le nombre d'étoiles de votre hébergement (facultatif)"> 
							<label>Nombre d'étoiles de votre hébergement (facultatif) :  </label>
							<input id="nb_etoiles" name="nb_etoiles" type="number" min="0" max="5"/>
						</p>
						
						<p title="Indiquez si du matériel est requis de la part des clients pour dormir dans votre hébergement (facultatif)">
							<label for="materiel">Matériel nécessaire :</label>
							<input type="text" name="materiel" id="materiel" size="75" maxlength="60" placeholder="Votre matériel requis (facultatif)"/>
						</p>
					</fieldset>
					
					<fieldset class="disabled" id="gestion_r" style="display:none;" disabled> 
						<label>Catégorie : </label>
						<select name="soustype" id="soustype" >
							<option value="bar">Bar</option>
							<option value="restaurant">Restaurant</option>
							<option value="snack">Snack</option>
							<option value="autre_restauration">Autre</option>
						</select>
						
						<p  title="Donnez l'adresse de votre établissement"> 
							<label for="description">Adresse : *</label>
							<textarea name="lieu" id="lieu" rows="2" cols="50" style="display:inline-block;" placeholder="L'adresse de votre établissement (1000 caractères maximum)" maxlength="1000" required></textarea>
						</p>
						
						<p title="Entrez le nombre d'étoiles de votre établissement (facultatif)"> 
							<label>Nombre d'étoiles de votre établissement (facultatif) :  </label>
							<input id="nb_etoiles" name="nb_etoiles" type="number" min="0" max="5"/>
						</p>
					</fieldset>

                </section>
                
                <input type="submit" value="Créer annonce" />
                <!--<input type="submit" formaction="/projet/commun2/annonce/creation_seance.php" value="Créer annonce et ajouter des séances" />-->
            </fieldset>
        </form>

		
		<a href="?q=moncompte">Retour au compte</a>
        <script src="/projet/commun2/annonce/miseajour-formulaire.js">
			
        </script>
		
<?php } ?>		