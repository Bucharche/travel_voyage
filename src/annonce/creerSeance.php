	<?php 
	include "/var/www/clients/client1/web3/web/projet/commun2/compte/util.php";
		$etat = estConnecte();
	if ($etat==0){ // non connecté
		echo "<p>Vous devez être connecté pour accéder à cette page.";
		echo "<a href= \"?q=connexion\" >Se connecter   </a>" ;
		echo "<a href=\"?q=inscription\" > S'inscrire </a>";
	}else if ($etat<0){ // cookie non valide
		echo "<p>Je sais que tu as titillé tes cookies, petit vilain (ou que la base de données a des problèmes...)</p>";
	}else if ($etat==1) { 
		echo "<p>Vous devez posséder un compte prestataire pour accéder à cette page</p>";
	}else if($etat==2){
	?>
        <h2>Création de séances</h2>
<link rel="stylesheet" type="text/css" href="design.css" />
        <form method="post" action="creation_seance.php">
            <fieldset >
                <legend style="font-size: large"> Ajoutez des séances à votre annonce  </legend>
                
				<p>
                    <label for="dateDebut">Date de début : *</label>
					<input type="text" name="dateDebut" onclick="ds_sh(this);" required />
                    
				</p>
				
				<p>
                    <label for="dateFin">Date de fin : *</label>
                    <input type="text" name="dateFin" onclick="ds_sh(this);" required />
				</p>
				
				<p>
                    <label for="heureDebut">Heure de début : *</label>
                    <input type="number" name="heureDebut" id="heureDebut" min="0" max="23" value="14" required/>h
					<input type="number" name="minDebut" id="minDebut" min="0" max="59" value="00" required/>
				</p>
				
				<p>
                    <label for="heureFin">Heure de fin : *</label>
                    <input type="number" name="heureFin" id="heureFin" min="0" max="23" value="15" required/>h
					<input type="number" name="minFin" id="minFin" min="0" max="59" value="00" required/>
				</p>
 
                <p  title="Donnez le lieu de départ(1000 caractères maximum)"> 
                    <label for="lieuDepart">Lieu de départ : *</label>
                    <textarea name="lieuDepart" id="lieuDepart" rows="1" cols="50" style="display:inline-block;" placeholder="Donnez le lieu de départ (1000 caractères maximum)" maxlength="1000" required></textarea>
                </p> 
				
				<p  title="Donnez le lieu d'arrivée (1000 caractères maximum)"> 
                    <label for="lieuArrivee">Lieu d'arrivée : *</label>
                    <textarea name="lieuArrivee" id="lieuArrivee" rows="1" cols="50" style="display:inline-block;" placeholder="Donnez le lieu d'arrivée (1000 caractères maximum)" maxlength="1000" required></textarea>
                </p>

               <p>
                    <label for="dureeMinH">Durée minimum : *</label>
                    <input type="number" name="dureeMinH" id="dureeMinH" min="0" max="23" value="1" required/>h
					<input type="number" name="dureeMinM" id="dureeMinM" min="0" max="59" value="00" required/>
				</p>
				
				<p>
                    <label for="dureeMaxH">Durée maximum : *</label>
                    <input type="number" name="dureeMaxH" id="dureeMaxH" min="0" max="23" value="1" required/>h
					<input type="number" name="dureeMaxM" id="dureeMaxM" min="0" max="59" value="00" required/>
				</p>

                <p title="Entrez le nombre de places disponibles pour cette annonce : "> 
					<label>Nombre de places : *</label>
					<input  type="number" id="nbPlaces" name="nbPlaces" min="1" max="50" required/>
				</p> 
				
				<p title="Entrez le prix par personne pour cette séance : "> 
					<label>Prix : *</label>
					<input  type="number" id="prix" name="prix" min="1" max="1000" required/>
				</p> 
				                
				
				<p  title="Décrivez la séance que vous proposez (1000 caractères maximum)"> 
                    <label for="description">Description : (facultatif)</label>
                    <textarea name="description" id="description" rows="1" cols="50" style="display:inline-block;" placeholder="Votre description (1000 caractères maximum)" maxlength="1000"></textarea>
                </p>
				
					<table class="ds_box" cellpadding="0" cellspacing="0" id="ds_conclass" style="display: none;">
						<tr>
							<td id="ds_calclass"></td>
						</tr>
					</table>

                
                <input type="submit" value="Créer séances" />
            </fieldset>
        </form>

        <!-- <script src="/projet/commun2/annonce/miseajour-formulaire.js"></script>
		-->
		<script type="text/javascript" src="calendrier.js"></script>
		
<?php } ?>		