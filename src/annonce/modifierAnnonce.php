<?php
include "/var/www/clients/client1/web3/web/projet/commun2/compte/util.php";
$etat = estConnecte();
if ($etat==0){ // non connecté
	echo "<p>Vous devez être connecté pour accéder à cette page.";
	echo "<a href= \"?q=connexion\" >Se connecter   </a>" ;
	echo "<a href=\"?q=inscription\" > S'inscrire </a>";
}else if ($etat<0){ // cookie non valide
	echo "Je sais que tu as titillé tes cookies, petit vilain (ou que la base de données a des problèmes...)";
}else if ($etat==2) { // connecté comme prestataire
	try{
		$bdd = new PDO('mysql:host=localhost;dbname=projet;charset=utf8', 'projet', 'tejorp');
	}catch(Exception $e){
		die('Erreur : '.$e->getMessage());
	}
 if (isset($_GET['id']) && $_GET['id'] != ""){ 
	setCookie('id_annonce',$_GET['id'],(time()+ 365*24*3600), '/');
	
 } else {
	
 }
		
 // données en post ?
	if (empty($_POST)) {   // pas de données en POST donc on affiche l'annonce demandée
	// on vérifie que le presta accède à son annonce à lui
	$reqVerif = $bdd->query("SELECT idPrestataires FROM annonces WHERE id='$_COOKIE[id_annonce]'");
	$res = $reqVerif->fetch();
	
	// si c'est son annonce on l'affiche
	if($res[0] == $_COOKIE['id_util']) {
		$req = $bdd->query("SELECT * FROM annonces WHERE id='$_COOKIE[id_annonce]' AND idPrestataires='$_COOKIE[id_util]'");
		$tab = $req->fetch(PDO::FETCH_ASSOC);
		
		?>
		<h1>Mon annonce</h1>
		<form method="post" action="?q=modifierA&amp;id=<?php echo $_COOKIE['id_annonce']; ?>">   	
						
			<p title="Donnez un nom à votre annonce">
				<label for="nom">Intitulé : </label>
				<input type="text" name="nom" id="nom" placeholder="Intitulé" value="<?php echo $tab['nom']?>"/>
			</p>
			
			<p  title="Décrivez l'annonce que vous proposez (1000 caractères maximum)"> 
				<label for="description">Description : </label>
				<textarea name="description" rows="1" cols="50" id="description" placeholder="Votre description (1000 caractères maximum)" maxlength="1000"/> <?php echo $tab['description'];?></textarea>
			</p>
			
			
			<p title="Type d'annonce que vous proposez">
				<label>Type : </label>
				<span>
				<?php
					$val = $tab['typeservice'];
					if($val = "activite") echo "Activité";
					else if ($val ="hebergement") echo "Hébergement";
					else if ($val = "restauration") echo "Restauration";
					else if ($val = "transport") echo "Transport";
					?>
					
				</span>
			</p>
			
			<p title="Catégorie d'annonce que vous proposez">
				<label>Catégorie : </label>
				<span>
				<?php
					switch ($tab['soustype']) {
						case "culturelle":
							echo "Culturelle";
							break;
						case "loisir":
							echo "Loisir";
							break;
						case "sportive":
							echo "Sportive";
							break;
						case "auberge":
							echo "Auberge de jeunesse";
							break;
						case "camping":
							echo "Camping";
							break;
						case "refuge":
							echo "Refuge";
							break;
						case "hotel":
							echo "Hôtel";
							break;
						case "gite":
							echo "Gîte";
							break;
						case "chambres":
							echo "Chambres d'hôtes";
							break;
						case "bar":
							echo "Bar";
							break;
						case "restaurant":
							echo "Restaurant";
							break;
						case "snack":
							echo "Snack";
							break;
						default :
							echo "Autre";
							break;
					} ?>
				</span>
			</p>
			
			<?php
			
			// gestion des activités
			if ($tab['typeservice'] == "activite") {
				
				// horaires libres ?
				if($tab['horairelibre'] == true) $libre = true; else $libre = false;
				echo '<p title="Indiquez si l\'activité que vous proposez a des horaires libres (le client peut venir à l\'heure qu\'il souhaite) ">
							<label >Horaires libres : </label>
							<input type="radio" name="horairelibre" value="oui" id="libre_oui" '; if($libre) echo 'checked'; echo '/> <label for="libre_oui">Oui</label>
							<input type="radio" name="horairelibre" value="non" id="libre_non" '; if(!$libre) echo 'checked'; echo ' /> <label for="libre_non">Non</label>
						</p>';
				
				
				
				
				// activite sportive ?
				if ($tab['soustype'] == "sportive") {
					$req_niveau = $bdd->query('SELECT niveau FROM sports WHERE id=\''.$_COOKIE['id_annonce'].'\'');
					$rep_niveau = $req_niveau->fetch();
					
					switch ($rep_niveau[0]) {
						case "pourTous": $sport="pourtous";
						break;
						
						case "debutant" : $sport="debutant";
						break;
						
						case "confirme" : $sport="confirme";
						break;
						
						case "expert" : $sport="expert";
						break;
						
						default : $sport="pourtous";
					}
					echo '
					<p title="Difficulté de votre activité">
						<label >Difficulté : </label>
						<input type="radio" name="niveau" value="pourTous" id="niveau_tous"';
					if($sport == "pourtous") echo 'checked'; echo ' /> <label for="niveau_tous">Pour tous</label>
					
					<input type="radio" name="niveau" value="debutant" id="niveau_deb" ';
					if($sport == "debutant") echo 'checked'; echo' /> <label for="niveau_deb">Débutant</label>
					
					<input type="radio" name="niveau" value="confirme" id="niveau_conf"';
					if($sport == "confirme") echo 'checked'; echo ' /> <label for="niveau_conf">Confirmé</label>
					
					<input type="radio" name="niveau" value="expert" id="niveau_exp"';
					if($sport == "expert")  echo 'checked'; echo '  /> <label for="niveau_exp">Expert</label>
					</p>';
				}
			}
			
			// gestion des hébergements
			if ($tab['typeservice'] == "hebergement") {
			
				$req_hebergement = $bdd->query("SELECT * FROM hebergements WHERE id='$_COOKIE[id_annonce]' ");
				$tab_hebergement = $req_hebergement->fetch(PDO::FETCH_ASSOC);
				
				echo '<p  title="Donnez l\'adresse de votre établissement"> 
								<label for="description">Adresse : </label>
								<textarea name="lieu" id="lieu" rows="1" cols="50" style="display:inline-block;" placeholder="L\'adresse de votre établissement (1000 caractères maximum)" maxlength="1000" required>';
								echo $tab_hebergement['lieu']; echo '</textarea>
							</p>';
				
				//echo "<pre>"; var_dump($tab_hebergement); echo "</pre>";
				echo '<p title="Entrez le nombre de places disponibles dans votre hébergement (minimum : 1, maximum : 50)"> 
							<label>Nombre de places proposées : </label>
							<input value="'; echo $tab_hebergement['nbPlaces']; echo'" id="nb_places" name="nb_places"type="number" min="1" max="50" required/>
						</p>';
						
				echo '<p title="Entrez le nombre d\'étoiles de votre hébergement (facultatif)"> 
							<label>Nombre d\'étoiles de votre hébergement (facultatif) :  </label>
							<input id="nb_etoiles" name="nb_etoiles" type="number" min="0" max="5" value="'; echo $tab_hebergement['nbEtoiles']; echo '"/>
						</p>';
			}
			
			// gestion des restaurations
			if($tab['typeservice'] ==  "restauration" ) {
			
				$req_restauration = $bdd->query("SELECT * FROM restaurations WHERE id='$_COOKIE[id_annonce]' ");
				$tab_restauration = $req_restauration->fetch(PDO::FETCH_ASSOC);
			
				echo '<p  title="Donnez l\'adresse de votre établissement"> 
							<label for="description">Adresse : *</label>
							<textarea name="lieu" id="lieu" rows="1" cols="50" style="display:inline-block;" placeholder="L\'adresse de votre établissement (1000 caractères maximum)" maxlength="1000" required>'; echo $tab_restauration['lieu']; echo '</textarea>
						</p>
						
						<p title="Entrez le nombre d\'étoiles de votre établissement (facultatif)"> 
							<label>Nombre d\'étoiles de votre établissement (facultatif) :  </label>
							<input id="nb_etoiles" name="nb_etoiles" type="number" min="0" max="5" value= "'; echo $tab_restauration['nbEtoiles']; echo '"/>
						</p>';
			}
			
			
			// materiel ? (activite ou hébergement)
			if ($tab['typeservice'] == "activite" || $tab['typeservice'] == "hebergement") {
				if(isset($tab['materiel'])) {
					echo '<p title="Indiquez si du matériel est requis de la part des clients pour participer à votre activité (facultatif)">
							<label for="materiel">Matériel nécessaire : </label>
							<input type="text" name="materiel" id="materiel" size="75" maxlength="60" placeholder="Votre matériel requis (facultatif)" value="'; echo $tab['materiel']; echo '"/>
						</p>';
				} 
			
			}

			?>
			
			<input type="submit" value="Valider les modifications"/>
		</form>
						
		
		<a id="modifierA" href="?q=voirA">Toutes mes annonces</a>
		
		
	<?php	
		
	} // sinon ça n'est pas l'annonce du presta connecté ; il ne la modifie pas
	else {
		echo "<p>Vous ne pouvez pas modifier une annonce que vous n'avez pas créé ! <p> <a href=\"?q=moncompte\"> Retour au compte </a>";
	}
 ?>

		
	
<?php
  } else {  // si donnnées en post (modification de l'annonce)
	// on vérifie que le presta modifie son annonce à lui 
	
	$reqVerif = $bdd->query("SELECT idPrestataires FROM annonces WHERE id='$_COOKIE[id_annonce]'");
	$res = $reqVerif->fetch();
	
	// si c'est son annonce on l'affiche
	if($res[0] == $_COOKIE['id_util']) {

		if(!empty($_POST["nom"])){
			echo 'update annonces set nom=\''.$_POST["nom"].'\' where id=\''.$_COOKIE['id_annonce'].'\'';
			$upd_nom = $bdd->prepare('update annonces set nom=\''.$_POST["nom"].'\' where id=\''.$_COOKIE['id_annonce'].'\'');
			$upd_nom->execute();
		}
		
		if(!empty($_POST["description"])){
			echo 
			$upd_description = $bdd->prepare('update annonces set description=\''.$_POST["description"].'\' where id=\''.$_COOKIE['id_annonce'].'\'');
			$upd_description->execute();
		}
		
		if(!empty($_POST["datenais"])){
			$upd_datenais = $bdd->prepare('update vacanciers set dateDeNaissance=\''.$_POST["datenais"].'\' where id=\''.$_COOKIE['id_util'].'\'');
			$upd_datenais->execute();
		}	
		
		if(!empty($_POST["adresse"])){
			$upd_adresse = $bdd->prepare('update comptes set adresse=\''.$_POST["adresse"].'\' where id=\''.$_COOKIE['id_util'].'\'');
			$upd_adresse->execute();
		}

		if(!empty($_POST["societe"])){
			$upd_societe = $bdd->prepare('update prestataires set societe=\''.$_POST["societe"].'\' where id=\''.$_COOKIE['id_util'].'\'');
			$upd_societe->execute();
		}	
		
		if(!empty($_POST["siret"])){
			$upd_siret = $bdd->prepare('update prestataires set numSiret=\''.$_POST["siret"].'\' where id=\''.$_COOKIE['id_util'].'\'');
			$upd_siret->execute();
		}	
		
		if(!empty($_POST["idPaypal"])){
			$upd_idPaypal = $bdd->prepare('update prestataires set idPaypal=\''.$_POST["idPaypal"].'\' where id=\''.$_COOKIE['id_util'].'\'');
			$upd_idPaypal->execute();
		}
		
		if(!empty($_POST["tel"])){
			$upd_tel = $bdd->prepare('update prestataires set tel=\''.$_POST["tel"].'\' where id=\''.$_COOKIE['id_util'].'\'');
			$upd_tel->execute();
		}	
		
		echo "<p>Vos modifications ont été prises en compte. <3 </p>";	
		//echo "<p> id annonce : $_COOKIE[id_annonce] </p>";
		//echo("<script type=\"text/javascript\">setTimeout(\"location.href = '?q=modifierA&amp;id=$_COOKIE[id_annonce]';\",9000);</script>");
		
	} else { // les données en POST viennent d'un mauvais presta !
		echo "<p>Vous ne pouvez pas modifier une annonce que vous n'avez pas créé ! <p> <a href=\"?q=moncompte\"> Retour au compte </a>";
	} 
}

} //ce n'est même pas un presta; il ne peut rien modifer
	else if ($etat == 1) {
	echo "<p>Vous devez être connecté en tant que prestataire pour accéder à cette page.";
	echo "<a href= \"?q=connexion\" >Se connecter   </a>" ;
	echo "<a href=\"?q=inscription\" > S'inscrire </a>";

} ?>