<?php
	
	$soustypes = array(
		"Famille" => "famille",
		"Top Eco !" => "top eco",
		"Sportif" => "sportif",
		"Xtrem" => "xtrem",
		"D&eacute;couverte" => "decouverte",
		"Pour les enfants" => "pour les enfants",
		"Nature" => "nature",
		"Autres" => "autre"
	);
	
	if(isset($_GET['s']) && in_array($_GET['s'], $soustypes)){ // 'S' pour Sous-Type
		
		// Gros bloc : Si on a re�u un type particulier � afficher :
		
		try{
			$bdd = new PDO('mysql:host=localhost;dbname=projet;charset=utf8', 'projet', 'tejorp');
		}catch(Exception $e){ die('Erreur : '.$e->getMessage()); }
		
		$reponse = $bdd->query("select * from `annonces` where typeservice='packs' and soustype='".$_GET['s']."' order by nom");
		
		if($reponse && $reponse->rowCount()>0){ //Si la requ�te a renvoy� quelque chose
			
			$nbParPage = isset($_GET["nbParPage"]) ? $_GET["nbParPage"] : 10;
			$index = isset($_GET["index"]) ? $_GET["index"] : 0;
			
			echo("<nav>Page : ");
			for($nbPage=0; $nbPage*$nbParPage<$reponse->rowCount(); $nbPage++){
				echo("<a href=\"?q=nospacks&amp;s=".$_GET['s']."&amp;index=".($nbPage*$nbParPage)."&amp;nbParPage=$nbParPage\">".($nbPage+1)." </a>");
			}
			?>
			<form action="?" method="GET">
				<label for="nbParPage">Nombre par page : </label>
				<input id="nbParPage" name="nbParPage" type="number" min="1" value="<?php echo($nbParPage); ?>" />
				<input id="index" style="display : none;" name="index" type="text" value="0" />
				<input id="q" style="display : none;" name="q" type="text" value="<?php echo($_GET['q']); ?>" />
				<input id="t" style="display : none;" name="t" type="text" value="<?php echo($_GET['t']); ?>" />
				<input id="s" style="display : none;" name="s" type="text" value="<?php echo($_GET['s']); ?>" />
				<input type="submit" id="ok" value="Appliquer" />
			</form>
			<?php // Ci-dessus, les champs q t s et index sont l) pour avoir les bons param�tres dans le GET
			
			echo("</nav>");
			
			for($init=0; $init<$index; $init++){ // On avance jusqu'� l'index de d�part souhait�
				$temp = $reponse->fetch();
			}
			
			while($donnees = $reponse->fetch()){
				if($nbParPage>0){
					echo("<a href=\""."?q=nospacks&amp;id=".$donnees['id']."\">\n");
					echo("\t<h2>".$donnees['nom']."</h2>\n");
					echo("\t<p>".$donnees['description']."</p>\n");
					echo("</a>\n");
				}
				$nbParPage--;
			}
			
		}else{
			echo("<p>Pas de packs pour cette cat&eacute;gorie. <a href=\"?q=nospacks\">Retour</a> </p>");
		}
		
	}else{
		
		// Sinon : on affiche toutes les cat�gories en lien cliquable
	
		echo("<section>\n<h2>Packs :</h2>\n");
		foreach($soustypes as $Saffiche => $soustype){
			echo("<a href=\"?q=nospacks&amp;s=$soustype\">$Saffiche</a>\n");
		}
		echo("</section>");
		
	}
?>