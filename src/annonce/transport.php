<?php
	$soustypes = array(
		"Train" => "train",
		"Covoiturage" => "covoiturage",
		"Avion" => "avion",
		"Autres" => "autre"
	);
	
	if(isset($_GET['s']) && in_array($_GET['s'], $soustypes)){ // 'S' pour Sous-Type
		
		// Gros bloc : Si on a reçu un type particulier à afficher :
		
		try{
			$bdd = new PDO('mysql:host=localhost;dbname=projet;charset=utf8', 'projet', 'tejorp');
		}catch(Exception $e){ die('Erreur : '.$e->getMessage()); }
		
		$reponse = $bdd->query("select * from `annonces` where typeservice='transport' and soustype='".$_GET['s']."' order by nom");
		
		if($reponse && $reponse->rowCount()>0){ //Si la requête a renvoyé quelque chose
			
			$nbParPage = isset($_GET["nbParPage"]) ? $_GET["nbParPage"] : 10;
			$index = isset($_GET["index"]) ? $_GET["index"] : 0;
			
			echo("<nav>Page : ");
			for($nbPage=0; $nbPage*$nbParPage<$reponse->rowCount(); $nbPage++){
				echo("<a href=\"?q=transport=".$_GET['s']."&amp;index=".($nbPage*$nbParPage)."&amp;nbParPage=$nbParPage\">".($nbPage+1)." </a>");
			} 
			?>
			<form action="?" method="GET">
				<label for="nbParPage">Nombre par page : </label>
				<input id="nbParPage" name="nbParPage" type="number" min="1" value="<?php echo($nbParPage); ?>" />
				<input id="index" style="display : none;" name="index" type="text" value="0" />
				<input id="q" style="display : none;" name="q" type="text" value="<?php echo($_GET['q']); ?>" />
				<input id="t" style="display : none;" name="t" type="text" value="<?php echo(isset($_GET['t'])?$_GET['t']:0); ?>" />
				<input id="s" style="display : none;" name="s" type="text" value="<?php echo($_GET['s']); ?>" />
				<input type="submit" id="ok" value="Appliquer" />
			</form>
			<?php // Ci-dessus, les champs q t s et index sont l) pour avoir les bons paramètres dans le GET
			
			echo("</nav>");
			
			for($init=0; $init<$index; $init++){ // On avance jusqu'à l'index de départ souhaité
				$temp = $reponse->fetch();
			}
			
			while($donnees = $reponse->fetch()){
				if($nbParPage>0){
					
					echo("<a href=\""."?q=annonce&amp;id=".$donnees['id']."\">\n");
					echo("\t<h2>".$donnees['nom']."</h2>\n");
					echo("\t<p>".$donnees['description']."</p>\n");
					echo("</a>\n");
				}
				$nbParPage--;
			}
			
		}else{
			echo("<p>Pas de transport pour cette catégorie. <a href=\"?q=transport\">Retour</a> </p>");
		}				}else{
		
		// Sinon : on affiche toutes les catégories en lien cliquable

		echo("<section>\n<h2>Transports</h2>\n");
		foreach($soustypes as $Saffiche => $soustype){
			echo("<a href=\"?q=transport&amp;s=$soustype\">$Saffiche</a>\n");
		}
		echo("</section>");
		
		
	}
?>