<?php
include "util.php";
$etat = estConnecte();
if ($etat==0){ // non connecté
	echo "<p>Vous devez être connecté pour accéder à cette page.";
	echo "<a href= \"?q=connexion\" >Se connecter   </a>" ;
	echo "<a href=\"?q=inscription\" > S'inscrire </a>";
}else if ($etat<0){ // cookie non valide
	echo "Je sais que tu as titillé tes cookies, petit vilain (ou que la base de données a des problèmes...)";
}else if ($etat>0) { // connecté comme client ou prestataire
	try{
		$bdd = new PDO('mysql:host=localhost;dbname=projet', 'projet', 'tejorp');
	}catch(Exception $e){
		die('Erreur : '.$e->getMessage());
	}
	$id=$_COOKIE["id_util"] ;
	

?> 

<?php // données en post ?
	if (empty($_POST)){ // pas de données en POST 
		$req_compte = $bdd->query("SELECT id,email,pseudo,nom, prenom,adresse FROM comptes WHERE id=$id");
		$tab_compte = $req_compte->fetch(PDO::FETCH_ASSOC);
?>

		<h1>Mon compte <?php if($etat==1){echo "client";}else{ echo "prestataire";} ?></h1>
		<div>
			<h2>Mon avatar </h2>
			<img src="?q=cat.jpg" alt="avatar"/>
			<p>Modifier mon avatar (à venir)</p>
		</div>

			
			<h2>Mes informations </h2>
			<form method="post" action="?q=moncompte">   	
				<fieldset>
					<legend>Mes identifiants</legend>
					<p>Mon pseudo :  <?php echo $tab_compte['pseudo']; ?>  </p>
					<p>Mon e-mail :  <?php  echo $tab_compte['email'];?> </p>
					<p>Modifier mon mot de passe : <a href="?q=modifpass">cliquer ici. </a></p> 
				</fieldset>
						   
				<fieldset>
					<legend>Mes informations personnelles</legend>
					<label for="nom">Mon nom : </label><input type="text" name="nom" id="nom" placeholder="Nom" value="<?php echo $tab_compte['nom']; ?>" />   
					<label for="prenom">Mon prénom : </label><input type="text" name="prenom" id="prenom" placeholder="Prénom" value="<?php echo $tab_compte['prenom'] ?>"/>
																														
					<?php	if ($etat==1){		
					$req_vac = $bdd->query("select dateDeNaissance from vacanciers where id=$id");
					$tab_vac = $req_vac->fetch(PDO::FETCH_ASSOC);
					?>
					 <label for="datenais">Ma date de naissance : </label><input type="text" name="dateNaissance" id="datenais"  value="<?php echo $tab_vac['dateDeNaissance'] ?>"/>		

					<?php } ?>
																																
					<label for="adresse">Mon adresse : </label><input type="text" name="adresse" id="adresse" placeholder="Adresse postale" value="<?php 
																	$req_adresse = $bdd->query('SELECT adresse FROM comptes WHERE id=\''.$_COOKIE['id_util'].'\'');
																	$rep_adresse = $req_adresse->fetch();
																	if($rep_adresse){
																		echo $rep_adresse[0];
																	}
																																					?>"/>
																																					
					<?php if($etat==2){		?>																														
								<label for="societe">Nom de la société : </label><input type="text" name="societe" id="societe" placeholder="Nom de la société" value="<?php 
																	$req_societe = $bdd->query('SELECT societe FROM prestataires WHERE id=\''.$_COOKIE['id_util'].'\'');
																	$rep_societe = $req_societe->fetch();
																	if($rep_societe){
																		echo $rep_societe[0];
																	}
																																										?>"/>
								<label for="siret">Numero Siret : </label><input type="text" name="siret" id="siret" placeholder="Numéro Siret" value="<?php 
																	$req_siret = $bdd->query('SELECT numSiret FROM prestataires WHERE id=\''.$_COOKIE['id_util'].'\'');
																	$rep_siret = $req_siret->fetch();
																	if($rep_siret){
																		echo $rep_siret[0];
																	}
																																										?>"/>
								<label for="idPaypal">Identifiant Paypal : </label><input type="text" name="idPaypal" id="idPaypal" placeholder="Identifiant Paypal" value="<?php 
																	$req_idPaypal = $bdd->query('SELECT idPaypal FROM prestataires WHERE id=\''.$_COOKIE['id_util'].'\'');
																	$rep_idPaypal = $req_idPaypal->fetch();
																	if($rep_idPaypal){
																		echo $rep_idPaypal[0];
																	}
																																										?>"/>
								<label for="tel">Numéro de téléphone : </label><input type="text" name="tel" id="tel" placeholder="Numéro de téléphone" value="<?php 
																	$req_tel = $bdd->query('SELECT tel FROM prestataires WHERE id=\''.$_COOKIE['id_util'].'\'');
																	$rep_tel = $req_tel->fetch();
																	if($rep_tel){
																		echo $rep_tel[0];
																	}
																																										?>"/>
								
					<?php } ?>		
				</fieldset>
				<input type="submit" value="Valider les modifications"/>
			</form>
			
			<?php if($etat==2){		?>		
			<div>
				<h2> Gérer mes annonces </h2>
				<p><a href="?q=voirA">Voir toutes vos annonces</a></p>
				<p><a href="?q=creerA">Créer une annonce</a></p>	
				<!--<p><a href="?q=modifierA">Cliquez ici pour modifier vos annonces</a></p>
				<p><a href="?q=supprimerA">Cliquez ici pour supprimer vos annonces</a></p>	-->			
			</div>
			<?php } ?>	
			
			<div>
				<h3>Mes amis </h3>
				<?php
					//affichage des amis quand l'id du compte est id1
					$req_amis = $bdd->query('SELECT id2 FROM amis WHERE id1=\''.$_COOKIE['id_util'].'\'');
					$rep_amis = $req_amis->fetchAll() ;
					$pasami = true;
					if ($rep_amis){
						foreach ($rep_amis as $value){
							if($rep_amis){
								$req_pseudo_amis = $bdd->query('SELECT pseudo FROM comptes WHERE id=\''.$value[0].'\'');
								$rep_pseudo_amis = $req_pseudo_amis->fetch() ;
								echo "<p>".$rep_pseudo_amis[0]."</p>";
								$pasami=false ;
							}
						}
					}
					
					//affichage des amis quand l'id du compte est id2
						$req_amis = $bdd->query('SELECT id1 FROM amis WHERE id2=\''.$_COOKIE['id_util'].'\'');
						$rep_amis = $req_amis->fetchAll() ;
						if ($rep_amis){
							foreach ($rep_amis as $value){
							if($rep_amis){
								$req_pseudo_amis = $bdd->query('SELECT pseudo FROM comptes WHERE id=\''.$value[0].'\'');
								$rep_pseudo_amis = $req_pseudo_amis->fetch() ;
								echo "<p>".$rep_pseudo_amis[0]."</p>";
							}
						}
						}else {
							if ($pasami==true){
								echo "<p>Vous n'avez pas d'amis. :'( mais ça viendra !</p>" ;
							}
						} 
				?>		
			</div>
	
<?php
 }else{  // si donnnées en post (modification du profil)

	if(!empty($_POST["nom"])){
		$upd_nom = $bdd->prepare('update comptes set nom=\''.$_POST["nom"].'\' where id=\''.$_COOKIE['id_util'].'\'');
		$upd_nom->execute();
	}
	
	if(!empty($_POST["prenom"])){
		$upd_prenom = $bdd->prepare('update comptes set prenom=\''.$_POST["prenom"].'\' where id=\''.$_COOKIE['id_util'].'\'');
		$upd_prenom->execute();
	}
	
	if(!empty($_POST["datenais"])){
		$upd_datenais = $bdd->prepare('update vacanciers set dateDeNaissance=\''.$_POST["datenais"].'\' where id=\''.$_COOKIE['id_util'].'\'');
		$upd_datenais->execute();
	}	
	
	if(!empty($_POST["adresse"])){
		$upd_adresse = $bdd->prepare('update comptes set adresse=\''.$_POST["adresse"].'\' where id=\''.$_COOKIE['id_util'].'\'');
		$upd_adresse->execute();
	}

	if(!empty($_POST["societe"])){
		$upd_societe = $bdd->prepare('update prestataires set societe=\''.$_POST["societe"].'\' where id=\''.$_COOKIE['id_util'].'\'');
		$upd_societe->execute();
	}	
	
	if(!empty($_POST["siret"])){
		$upd_siret = $bdd->prepare('update prestataires set numSiret=\''.$_POST["siret"].'\' where id=\''.$_COOKIE['id_util'].'\'');
		$upd_siret->execute();
	}	
	
	if(!empty($_POST["idPaypal"])){
		$upd_idPaypal = $bdd->prepare('update prestataires set idPaypal=\''.$_POST["idPaypal"].'\' where id=\''.$_COOKIE['id_util'].'\'');
		$upd_idPaypal->execute();
	}
	
	if(!empty($_POST["tel"])){
		$upd_tel = $bdd->prepare('update prestataires set tel=\''.$_POST["tel"].'\' where id=\''.$_COOKIE['id_util'].'\'');
		$upd_tel->execute();
	}	
	
	echo "<p>Vos modifications ont été prises en compte. <3 </p>";	
	
	
}} ?>