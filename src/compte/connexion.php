<?php
	include "util.php";
	//est-il déjà connecté ?
	if (estConnecte()){ // déjà co
		echo "<p>Vous êtes déjà connecté... mais que faites-vous ici ?</p>";
	}else{ // pas co
			
		if(!isset($_POST["pseudo"]) || !isset($_POST["passe"])){ //si pas de données en POST
?>	
			<form  method="post" action="?q=connexion">
				<fieldset>
					<legend>Connexion</legend>
					<label for="pseudo">Nom d'utilisateur : </label><input type="text" name="pseudo" id="pseudo" placeholder="Pseudo" required/>
					<label for="mdp">Mot de passe : </label><input type="password" name="passe" id="mdp" placeholder="Mot de passe" required />           
				</fieldset>
				<input type="submit" value="Connexion"/>
				
				<p>Pas encore inscrit ? <a href="?q=inscription">M'inscrire </a></p>
			</form> 
<?php
		
		//si données en POST	
		}else{
			try{
				$bdd = new PDO('mysql:host=localhost;dbname=projet', 'projet', 'tejorp');
			}catch(Exception $e){
				die('Erreur : '.$e->getMessage());
			}	
			//le pseudo existe-t-il ?
			$req_pseudo = $bdd->prepare("select motdepasse,pseudo,id from comptes where pseudo= :pseudo ");
			$conv = array ("pseudo"=>$_POST["pseudo"]);
			$req_pseudo->execute($conv);
			$reponse_pseudo = $req_pseudo->fetch();

			if (isset($reponse_pseudo['pseudo'])){   // si pseudo trouvé 
				 // Si pseudo trouvé et mot de passe correspond
				   if(md5($_POST["passe"]) == $reponse_pseudo["motdepasse"] ){
						//déclaration des cookies
						setcookie('pseudo',$_POST['pseudo'],(time()+ 365*24*3600),'/');
						setcookie('mdp_util',md5($_POST["passe"]),(time()+ 365*24*3600),'/');
						setcookie('id_util',$reponse_pseudo['id'],(time()+ 365*24*3600),'/');
						echo "<p>Pseudo et mot de passe valide, vous êtes connecté(e) </p>";
						echo("<script type=\"text/javascript\">setTimeout(\"location.href = '?';\",2000);</script>");
					}else{ //mot de passe incorrect ou pseudo inconnu
						echo  "<p>Mot de passe erroné, réessayez.</p>";
					}
			}else{
				echo "<p>Pseudo inconnu, réessayez</p>" ;
			}
		}
    }

?>
