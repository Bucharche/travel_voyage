
<?php
include "util.php";
include "fonctions.js";
//est-il déjà connecté ?
if (estConnecte()){ // déjà co
	echo "<p>Vous êtes déjà connecté... mais que faites-vous ici ?</p>";
}else{ // pas co

	//test si données en POST
		if(!isset($_POST["pseudo"]) || !isset($_POST["passe"]) || !isset($_POST["email"]) || !isset($_POST["cond"])){
	//si pas de données (ou pas toutes les données attendues)en POST
	
?>
	
		<h1>Formulaire d'inscription vacancier</h1>
	
		<form method="post" action="?q=inscription">   
			
			<fieldset>   
				<legend>Informations de connexion</legend>       
				<label for="pseudo">Saisissez votre pseudo/identifiant : *</label><input type="text" name="pseudo" id="pseudo" placeholder="Pseudo" required/>
				<label for="mail">Adresse e-mail : *</label><input type="email" name="email" id="mail" placeholder="E-mail" required/>
				<label for="mdp">Mot de passe : (6 caractères minimum)*</label><input type="password" name="passe" id="mdp" placeholder="Mot de passe" onkeyup="checkLengthPass();" required /> 
				<div id="div_comp"></div>
				<label for="mdp2">Confirmation du mot de passe : *</label><input type="password" name="passe2" id="mdp2" placeholder="Mot de passe" onkeyup="checkPass();" required/>
				<div id="div_comp2"></div>
			</fieldset> 
			  
			<fieldset>    
				<legend>Informations personnelles</legend>       
				<label for="nom">Saisissez votre nom : </label><input type="text" name="nom" id="nom" placeholder="Nom" />   
				<label for="prenom">Saisissez votre prénom : </label><input type="text" name="prenom" id="prenom" placeholder="Prénom" />
				<label for="dateNaissance">Saisissez votre date de naissance : </label><input type="date" name="dateNaissance" id="dateNaissance" placeholder="jj/mm/aaaa" />
				<label for="adresse">Saisissez votre adresse : </label><input type="text" name="adresse" id="adresse" placeholder="Adresse postale" />
			</fieldset>

		
			<div>
				<p>* : Champ obligatoire</p>
				<div>  
					<input type="checkbox" name="cond" id="cond" required/>
					<label for="cond">J'accepte les conditions générales d'utilisation de Travel Voyage</label>
				</div>

				<input type="submit" value="M'inscrire"/>
	
				<p>Vous êtes déjà inscrit ? <a href="?q=connexion">Connectez-vous ici</a></p>

				<p><a href="?q=inscriptionpro">Cliquez ici pour créer un compte professionnel</a></p>
			</div>
			
		</form>   
	
	
	<?php
	}else{
	//traitement s'il existe des données en POST
		try{
			$bdd = new PDO('mysql:host=localhost;dbname=projet', 'projet', 'tejorp');
		}catch(Exception $e){
			die('Erreur : '.$e->getMessage());
		}
               //les champs obligatoires sont-ils complétés ?
               if(!isset($_POST["pseudo"]) || !isset($_POST["passe"]) || !isset($_POST["email"]) || !isset($_POST["cond"]) ){
                    echo "<p>Mais que fais-tu petit vilain ?</p>" ;
                }else{
					// vérification de l'unicité du pseudo
					$pseudo = $_POST['pseudo'] ;
					$verif_pseudo = $bdd->query('SELECT pseudo FROM comptes WHERE pseudo=\''.$pseudo.'\'');
					$exist_pseudo = $verif_pseudo->fetch();

					if($exist_pseudo){
						echo "Le pseudo que vous avez saisi n'est pas disponible, réessayez.";					
					}else{
						//vérification de l'unicité de l'email
						$email = $_POST['email'] ;
						$verif_email = $bdd->query('SELECT email FROM comptes WHERE email=\''.$email.'\'');
						$exist_email = $verif_email->fetch();

						if($exist_email){
							echo "L'email que vous avez saisi est déjà utilisée, réessayez.";						
						}else{ 
							//insertion dans Comptes
							$req=$bdd->prepare("INSERT INTO `comptes` (`id`, `email`, `pseudo`, `motdepasse`, `nom`, `prenom`, `adresse`) VALUES (:id, :email, :pseudo, :mdp, :nom, :prenom, :adresse)");
						
							//incrémentation de l'id_compte	
							$reponse = $bdd->query('SELECT max(id) FROM `comptes`');
							$donnees = $reponse->fetch();
							$id = $donnees['max(id)']+1;
							
							//tableau des paramètres pour la requête de création dans Comptes
							$conversion = array(
								"id" => $id,
								"email" => $_POST ["email"],
								"pseudo" => $_POST ["pseudo"],
								"mdp" => md5($_POST["passe"]),
								"nom" => $_POST ["nom"],
								"prenom" => $_POST ["prenom"],
								"adresse" => $_POST ["adresse"]
							);
							$req->execute($conversion);	
							
							//insertion dans Vacanciers
							$req_vac = $bdd->prepare("INSERT INTO `vacanciers`(`id`,`dateDeNaissance`) VALUES (:id, :dateNaissance)");
							$conversion_vac = array(
								"id" => $id,
								"dateNaissance" => $_POST ["dateNaissance"]
							) ;
							$req_vac->execute($conversion_vac);

							echo "<p>Votre inscription a bien été prise en compte, ".$_POST['pseudo']." =D </p>";
						
						}
					}
                }
		}	
	//Fin traitement si POST
	}
	?>    
