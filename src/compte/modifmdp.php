<?php
include "fonctions.js";
include "util.php";

$etat = estConnecte();
if ($etat==0){ // non connecté
	echo "<p>Vous devez être connecté pour accéder à cette page.";
	echo "<a href= \"?q=connexion\" >Se connecter   </a>" ;
	echo "<a href=\"?q=inscription\" > S'inscrire </a>";
}else if ($etat<0){ // cookie non valide
	echo "Je sais que tu as titillé tes cookies, petit vilain (ou que la base de données a des problèmes...)";
}else if ($etat>0) { // connecté comme client ou prestataire
	
	if (empty($_POST)){ //pas de données en post
?>

	<form method="post" action="?q=modifpass" >
		<h3>Modifier mon mot de passe</h3>
		<fieldset>	
			<legend>Modifier mon mot de passe </legend>
			<label for="oldpass">Mon ancien mot de passe : </label><input type="password" name="oldpass" id="oldpass" placeholder="Ancien mot de passe " required/>
			<label for="mdp">Mon nouveau mot de passe : (6 caractères minimum)*</label><input type="password" name="passe" id="mdp" placeholder="Mot de passe" onkeyup="checkLengthPass();" required /> 
			<div id="div_comp"></div>
			<label for="mdp2">Confirmer mon mot de passe : *</label><input type="password" name="passe2" id="mdp2" placeholder="Mot de passe" onkeyup="checkPass();" required/>
			<div id="div_comp2"></div>
		</fieldset>
		<input type="submit" value="Connexion"/>
	</form>


	<?php 

	}else{ //données en post
	//	var_dump($_POST);
		
		
		if (!isset($_POST["oldpass"]) || !isset($_POST["passe"]) || !isset($_POST["passe2"]) ){
			 echo "<p>Mais que fais-tu petit vilain ?</p>" ;
		}else{
			try{
				$bdd = new PDO('mysql:host=localhost;dbname=projet', 'projet', 'tejorp');
			}catch(Exception $e){
				die('Erreur : '.$e->getMessage());
			}
			
			$req_mdp = $bdd->prepare("select motdepasse from comptes where id=\"".$_COOKIE["id_util"]."\"");
			$req_mdp->execute();
			$reponse_mdp = $req_mdp->fetch();
			
			if(md5($_POST["oldpass"]) == $reponse_mdp["motdepasse"] ){
				
				$upd_pass = $bdd->prepare("update comptes set motdepasse=:passe where id=:id");
				
				$conv = array (
					"id" => $_COOKIE["id_util"],
					"passe" => md5($_POST["passe"])
				);
				
				$upd_pass->execute($conv);	
				//modif des cookies
				setcookie('mdp_util',md5($_POST["passe"]),(time()+ 365*24*3600),'/');
				echo "<p>Mot de passe modifié avec succès ! </p>";
				echo("<script type=\"text/javascript\">setTimeout(\"location.href = '?q=moncompte';\",2000);</script>");
			}else{ //mot de passe incorrect ou pseudo inconnu
				echo  "<p>Ancien mot de passe incorrect, recommencez</p>";
			}
			
		}
		
		
	}
}
?>
