-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Ven 09 Janvier 2015 à 16:19
-- Version du serveur :  5.5.40-0+wheezy1
-- Version de PHP :  5.4.34-0+deb7u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `projet`
--
CREATE DATABASE IF NOT EXISTS `projet` DEFAULT CHARACTER SET ascii COLLATE ascii_bin;
USE `projet`;

-- --------------------------------------------------------

--
-- Structure de la table `amis`
--

DROP TABLE IF EXISTS `amis`;
CREATE TABLE IF NOT EXISTS `amis` (
  `id1` smallint(5) unsigned NOT NULL,
  `id2` smallint(5) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vider la table avant d'insérer `amis`
--

TRUNCATE TABLE `amis`;
--
-- Contenu de la table `amis`
--

INSERT INTO `amis` (`id1`, `id2`) VALUES
(5, 6),
(0, 8),
(1, 10);

-- --------------------------------------------------------

--
-- Structure de la table `annonces`
--

DROP TABLE IF EXISTS `annonces`;
CREATE TABLE IF NOT EXISTS `annonces` (
  `id` smallint(5) unsigned NOT NULL,
  `idPrestataires` smallint(5) unsigned NOT NULL,
  `nom` char(255) NOT NULL,
  `description` text NOT NULL,
  `materiel` varchar(100) DEFAULT NULL,
  `typeservice` varchar(50) NOT NULL,
  `soustype` varchar(50) NOT NULL,
  `horairelibre` tinyint(1) NOT NULL,
  `tags` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vider la table avant d'insérer `annonces`
--

TRUNCATE TABLE `annonces`;
--
-- Contenu de la table `annonces`
--

INSERT INTO `annonces` (`id`, `idPrestataires`, `nom`, `description`, `materiel`, `typeservice`, `soustype`, `horairelibre`, `tags`) VALUES
(0, 2, 'Escalade accompagnée !', 'Une envie de voir la région d''en haut ? Un moniteur certifié vous guidera pour une journée inoubliable ! Niveau débutant', 'chaussures adaptées', 'activite', 'sportive', 0, 'sport,escalade,débutant'),
(1, 2, 'Escalade haut niveau', 'Plus qu''une simple ascension, c''est un concentré de sensations fortes qui vous attend !', 'chaussures adaptées', 'activite', 'sportive', 0, 'sport,escalade, confirmé, haut niveau,ascension,sensations'),
(2, 4, 'Cours de tennis', 'Cours de tennis en plein air pour toute la famille !', '', 'activite', 'sportive', 0, 'tennis, famille'),
(3, 7, 'Bar le Super', 'Un bar chaleureux et accueillant qui vous propose des boissons de la région !', '', 'restauration', 'bar', 0, 'bar,boissons'),
(4, 7, 'Restaurant La Brise Montagnarde', 'Un restaurant à la fraîcheur et à la jeunesse inimitable. Notre spécialité : la galinette cendrée !', '', 'restauration', 'restaurant', 1, 'restaurant,La Brise Montagnarde,spécialité'),
(5, 9, 'Snack Le KitKat', 'Notre spécialité : le KitKat du berger ! =)', '', 'restauration', 'snack', 1, 'le KitKat du berger ! ,spécialité'),
(6, 9, 'Hôtel les Trois Marmottes', 'Venez passer une nuit dans notre hôtel familial au décor d''antan et passez dire bonjour aux marmottes le lendemain matin !', '', 'hebergement', 'hotel', 1, 'hôtel les Deux Marmottes famille marmotte'),
(7, 9, 'Auberge de jeunesse Les Marmottons', 'Des dortoirs spacieux pour toute la famille !', 'des draps', 'hebergement', 'auberge', 1, 'Auberge de jeunesse Les Marmottons Des dortoirs spacieux pour toute la famille '),
(8, 4, 'Camping Les Colis Sauvages', 'Venez faire gambader votre coli en toute sérénité dans un décor idyllique !', 'une tente, un sac de couchage', 'hebergement', 'camping', 1, 'camping Les Colis Sauvages Venez faire gambader votre coli en toute sérénité dans un décor idyllique ! une tente, un sac de couchage'),
(9, 4, 'Chambres d''hôtes l''Edelweiss', 'Allez viens, on est bien :p', 'uniquement votre joie de vivre', 'hebergement', 'chambres', 1, 'Chambres d''hôtes l''Edelweiss Allez viens, on est bien :p uniquement votre joie de vivre'),
(10, 2, 'Refuge de l''Aigle', 'Refuge de haute montagne pour souffler après une dure journée au froid', 'votre matériel de couchage, votre nourriture', 'hebergement', 'refuge', 1, 'Refuge de l''Aigle Refuge de haute montagne pour souffler après une dure journée au froid votre matériel de couchage, votre nourriture'),
(11, 7, 'Restaurant Les deux Bouquetins', 'Super restaurant ! Notre spécialité : la fondue isèroise !', '', 'restauration', 'restaurant', 1, 'restaurant les deux bouetins spécialité'),
(12, 2, 'Hôtel La Shizu Sauvage', 'Venez passer une nuit dans notre hôtel familial avec votre shizu :p', '', 'hebergement', 'hotel', 1, 'hôtel La Shizu Sauvage Venez passer une nuit dans notre hôtel familial avec votre shizu :p famille hotel'),
(13, 3, 'Snacoli', 'Le snack où l''on peut manger des hot-coli, des coli-bagnat ou même des hamburcoli !', 'Aucun', 'restauration', 'snack', 1, 'coli, manger, om, nom, nom'),
(14, 2, 'Jacuzzi d''Allan', 'Venez prendre un bain avec Allan ! (activité cul-turelle !)', 'Votre combinaison de plongée', 'activite', 'loisir', 1, ' Hôtel d''Allan  Venez prendre un bain avec Allan !  activite  loisir  Votre combinaison de plongée  oui '),
(15, 4, 'Visite du musée', 'Muséum de l''histoire de la montagne', '', 'activite', 'culturelle', 0, 'musée, muséum, famille'),
(16, 3, 'LA rando d''EasyRando', 'Une superbe randonnée au travers des cols et pas ; le tout au milieu d''un décor pittoresque', 'de bonnes chaussures', 'activite', 'loisir', 0, ' LA rando d''EasyRando  Une superbe randonnée au travers des cols et pas ; le tout au milieu d''un décor pittoresque  activite  loisir  de bonnes chaussures  non '),
(17, 2, 'rando en famille', 'super belle rando', 'vos chaussures de marche', 'activite', 'sportive', 0, ' rando en famille  super belle rando  activite  sportive  vos chaussures de marche  non '),
(18, 2, 'Cours de piscine', 'super cool swimming pool !', 'votre maillot de bain, un bonnet de bain', 'activite', 'sportive', 0, ' Cours de piscine  super cool swimming pool !  activite  sportive  votre maillot de bain, un bonnet de bain  non '),
(19, 3, 'Exposé d''expression', 'trop du caca', 'le peu de cervea qu''il te reste', 'activite', 'loisir', 1, ' Exposé d''expression  trop du caca  activite  loisir  le peu de cervea qu''il te reste  oui '),
(20, 2, 'Rando avec Nico', 'Trop super !', 'un peu de bonne humer', 'activite', 'loisir', 1, ' Rando avec Nico  Trop super !  activite  loisir  un peu de bonne humer  oui '),
(21, 2, 'Rando avec Nico 2', 'Super cool !', 'Une Pelle', 'activite', 'loisir', 1, ' Rando avec Nico 2  Super cool !  activite  loisir  Une Pelle  oui '),
(22, 2, 'Randonnée avec les marmottes', 'Trop cool, c''est beau !', 'Chaussures', 'activite', 'loisir', 0, ' Randonnée avec les marmottes  Trop cool, c''est beau !  activite  loisir  Chaussures  non '),
(23, 2, 'Randonnée avec bouquetins', 'C''est trop cool !', 'Chaussres de rando', 'activite', 'sportive', 0, ' Randonnée avec bouquetins  C''est trop cool !  activite  sportive  Chaussres de rando  non '),
(25, 3, 'Train Grenoble - Chamonix', 'Train départ à Grenoble : 7h56\r\nArrivée à Chamonix : 10h24', NULL, 'transport', 'train', 0, 'train,annonce,sncf'),
(26, 2, 'Rafting dans le torrent du Moh', 'Une ballade formidable pour découvrir la beauté du torrent emblématique de la région. Sensations garanties !', '', 'activite', 'sportive', 0, ' Rafting dans le torrent du Moh  Une ballade formidable pour découvrir la beauté du torrent emblématique de la région. Sensations garanties !  activite  sportive    non  pourTous '),
(27, 2, 'Luge d''été', 'Amusez vous même en Août sur les pistes avec votre luge !\r\nRire garanti !', 'Votre luge', 'activite', 'sportive', 0, ' Luge d''été  Amusez vous même en Août sur les pistes avec votre luge !\r\nRire garanti !  activite  sportive  Votre luge  non  pourTous '),
(28, 2, 'Resto - Le petit jambonnet', 'Un petit jambon tout mimi vous accueille et vous fait découvrir de nouvelles saveurs !', '', 'restauration', 'restaurant', 1, ' Resto - Le petit jambonnet  Un petit jambon tout mimi vous accueille et vous fait découvrir de nouvelles saveurs !  restauration  restaurant  Ici et pas là bas !   '),
(29, 2, 'Super cours d''informatique avec William', 'J''adore &lt;3', 'Mon coeur !', 'activite', 'autre_activite', 1, ' Super cours d''informatique avec William  J''adore &lt;3  activite  autre_activite  Mon coeur !  oui '),
(30, 2, 'Super cours d''informatique avec William', 'Luv', '', 'activite', 'sportive', 1, ' Super cours d''informatique avec William  Luv  activite  sportive    oui  expert '),
(31, 2, 'Super cours d''équitation avec Débo !', 'le cheval, le cheval, c''est génial !', 'votre bombe', 'activite', 'loisir', 0, ' Super cours d''équitation avec Débo !  le cheval, le cheval, c''est génial !  activite  loisir  votre bombe  non '),
(32, 2, 'Toto', 'Avec toto !', 'Aucun', 'activite', 'loisir', 1, ' Toto  Avec toto !  activite  loisir  Aucun  oui ');

-- --------------------------------------------------------

--
-- Structure de la table `commentaires`
--

DROP TABLE IF EXISTS `commentaires`;
CREATE TABLE IF NOT EXISTS `commentaires` (
  `id` smallint(5) unsigned NOT NULL,
  `idCompte` smallint(5) unsigned NOT NULL,
  `idAnnonce` smallint(5) unsigned NOT NULL,
  `note` float unsigned NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vider la table avant d'insérer `commentaires`
--

TRUNCATE TABLE `commentaires`;
--
-- Contenu de la table `commentaires`
--

INSERT INTO `commentaires` (`id`, `idCompte`, `idAnnonce`, `note`, `description`) VALUES
(0, 0, 1, 5, 'Une ballade sensationelle, adaptée aux confirmés '),
(2, 5, 18, 4, 'Trop pool la swimming pool !'),
(3, 5, 18, 4, 'gugugugu'),
(4, 5, 27, 4, 'C''est super !');

-- --------------------------------------------------------

--
-- Structure de la table `comptes`
--

DROP TABLE IF EXISTS `comptes`;
CREATE TABLE IF NOT EXISTS `comptes` (
  `id` smallint(5) unsigned NOT NULL,
  `email` char(255) NOT NULL,
  `pseudo` char(255) NOT NULL,
  `motdepasse` char(255) NOT NULL,
  `nom` char(255) DEFAULT NULL,
  `prenom` char(255) DEFAULT NULL,
  `adresse` varchar(511) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vider la table avant d'insérer `comptes`
--

TRUNCATE TABLE `comptes`;
--
-- Contenu de la table `comptes`
--

INSERT INTO `comptes` (`id`, `email`, `pseudo`, `motdepasse`, `nom`, `prenom`, `adresse`) VALUES
(0, 'vac1@vac.fr', 'Colisan', 'b6edd10559b20cb0a3ddaeb15e5267cc', 'Violette', 'Nicolas', 'rue des Coli'),
(1, 'vac2@vac.fr', 'Shizu', '15be6c30a18e2e9baf739f457193fdaf', 'Hernandez', 'Celine', 'rue des Shizu'),
(2, 'prest1@prest.fr', 'Escalade4ever', 'b6edd10559b20cb0a3ddaeb15e5267cc', 'Dupont', 'GÃ©rard', '2 rue du haut mur'),
(3, 'prest2@prest.fr', 'EasyRando', 'b6edd10559b20cb0a3ddaeb15e5267cc', 'Martin', 'Pierre', 'rue du la randonnÃ©e'),
(4, 'prest3@prest.fr', 'TennisWoman', 'b6edd10559b20cb0a3ddaeb15e5267cc', 'Durand', 'Claire', 'rue du cours de tennis'),
(5, 'vac3@vac.fr', 'Juju', 'b6edd10559b20cb0a3ddaeb15e5267cc', 'Pelletier', 'Juliette', 'Rue des Juliette'),
(6, 'vac4@vac.fr', 'Pivil', 'b6edd10559b20cb0a3ddaeb15e5267cc', 'Pontonnier', 'RÃ©mi', 'rue des Roux'),
(7, 'prest4@prest.fr', 'SuperBar', 'b6edd10559b20cb0a3ddaeb15e5267cc', 'LeFou', 'Guigui', 'rue du torché'),
(8, 'vac5@vac.fr', 'ThoThoThomas', 'b6edd10559b20cb0a3ddaeb15e5267cc', 'Jalabert', 'Thomas', 'rue des Motus'),
(9, 'prest5@prest.fr', 'pseudo_prest5', 'b6edd10559b20cb0a3ddaeb15e5267cc', 'nom_prest5', 'prenom_prest5', 'rue du prest5'),
(10, 'vac3@vac.fr', 'Bucharche', 'b6edd10559b20cb0a3ddaeb15e5267cc', 'Bourrut', 'Yvan', 'rue des YvYv'),
(11, 'anonymous@gmail.com', 'anonymous', '25f9e794323b453885f5181f1b624d0b', 'TonCamarade', 'dePromo', 'Iut 2');

-- --------------------------------------------------------

--
-- Structure de la table `hebergements`
--

DROP TABLE IF EXISTS `hebergements`;
CREATE TABLE IF NOT EXISTS `hebergements` (
  `id` smallint(5) unsigned NOT NULL,
  `nbEtoiles` tinyint(4) DEFAULT NULL,
  `nbPlaces` smallint(6) NOT NULL,
  `lieu` varchar(10000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vider la table avant d'insérer `hebergements`
--

TRUNCATE TABLE `hebergements`;
--
-- Contenu de la table `hebergements`
--

INSERT INTO `hebergements` (`id`, `nbEtoiles`, `nbPlaces`, `lieu`) VALUES
(6, 2, 10, 'Allée de la Shizu'),
(7, 0, 10, 'Allée de la Juju'),
(8, 3, 20, 'Allée de Devil'),
(9, 3, 10, 'Allée de Thototomas'),
(10, 0, 8, 'Allée du Coli'),
(12, 5, 20, 'Allée de Bucharche');

-- --------------------------------------------------------

--
-- Structure de la table `packs`
--

DROP TABLE IF EXISTS `packs`;
CREATE TABLE IF NOT EXISTS `packs` (
  `idProgramme` smallint(5) unsigned NOT NULL,
  `desciption` text NOT NULL,
  `prix` double unsigned NOT NULL,
  `nbPack` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vider la table avant d'insérer `packs`
--

TRUNCATE TABLE `packs`;
--
-- Contenu de la table `packs`
--

INSERT INTO `packs` (`idProgramme`, `desciption`, `prix`, `nbPack`) VALUES
(2, 'Du Sport a gogo', 700, 10);

-- --------------------------------------------------------

--
-- Structure de la table `paniers`
--

DROP TABLE IF EXISTS `paniers`;
CREATE TABLE IF NOT EXISTS `paniers` (
  `idCompte` smallint(5) unsigned NOT NULL,
  `idAnnonce` smallint(5) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vider la table avant d'insérer `paniers`
--

TRUNCATE TABLE `paniers`;
--
-- Contenu de la table `paniers`
--

INSERT INTO `paniers` (`idCompte`, `idAnnonce`) VALUES
(0, 0),
(8, 0),
(0, 1),
(8, 1),
(10, 1),
(0, 2),
(5, 2),
(10, 2),
(0, 3),
(10, 3),
(0, 4),
(10, 4),
(10, 5),
(10, 6),
(5, 7),
(8, 7),
(10, 7),
(10, 8),
(10, 9),
(10, 10),
(5, 15),
(5, 18);

-- --------------------------------------------------------

--
-- Structure de la table `photos`
--

DROP TABLE IF EXISTS `photos`;
CREATE TABLE IF NOT EXISTS `photos` (
  `id` smallint(5) unsigned NOT NULL,
  `idCompte` smallint(5) unsigned NOT NULL,
  `idAnnonce` smallint(5) unsigned NOT NULL,
  `extension` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vider la table avant d'insérer `photos`
--

TRUNCATE TABLE `photos`;
--
-- Contenu de la table `photos`
--

INSERT INTO `photos` (`id`, `idCompte`, `idAnnonce`, `extension`) VALUES
(2, 2, 1, 'jpg'),
(5, 2, 1, 'jpg'),
(6, 4, 1, 'jpg'),
(7, 10, 1, 'jpg'),
(10, 5, 18, 'jpg'),
(11, 5, 27, 'png');

-- --------------------------------------------------------

--
-- Structure de la table `prestataires`
--

DROP TABLE IF EXISTS `prestataires`;
CREATE TABLE IF NOT EXISTS `prestataires` (
  `id` smallint(5) unsigned NOT NULL,
  `societe` varchar(500) NOT NULL,
  `numSiret` varchar(14) DEFAULT NULL,
  `idPaypal` varchar(500) NOT NULL,
  `tel` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vider la table avant d'insérer `prestataires`
--

TRUNCATE TABLE `prestataires`;
--
-- Contenu de la table `prestataires`
--

INSERT INTO `prestataires` (`id`, `societe`, `numSiret`, `idPaypal`, `tel`) VALUES
(2, 'Escalade4ever', '21R3T', 'escalade3000paypal', '01 02 03 04 05'),
(3, 'EasyRando', 'SIRET2', 'payPal2', '01 02 03 04 05'),
(4, 'TennisWoman', 'SIRET3', 'payPal3', '01 02 03 04 05'),
(7, 'SuperBar', 'SIRET4', 'payPal4', '01 02 03 04 05'),
(9, 'societe5', 'SIRET5', 'payPal5', '01 02 03 04 05');

-- --------------------------------------------------------

--
-- Structure de la table `programmes`
--

DROP TABLE IF EXISTS `programmes`;
CREATE TABLE IF NOT EXISTS `programmes` (
  `id` smallint(5) unsigned NOT NULL,
  `idCompte` smallint(5) unsigned DEFAULT NULL,
  `nom` varchar(50) NOT NULL,
  `dateDebut` date NOT NULL,
  `dateFin` date NOT NULL,
  `payeBloque` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vider la table avant d'insérer `programmes`
--

TRUNCATE TABLE `programmes`;
--
-- Contenu de la table `programmes`
--

INSERT INTO `programmes` (`id`, `idCompte`, `nom`, `dateDebut`, `dateFin`, `payeBloque`) VALUES
(0, 0, 'Vacances tranquille', '2014-12-06', '2014-12-13', 0),
(1, 5, 'Vacances sportive avec toutes ma famille', '2014-12-06', '2014-12-13', 1),
(2, NULL, 'Sportif', '2014-12-06', '2014-12-13', 0);

-- --------------------------------------------------------

--
-- Structure de la table `promotions`
--

DROP TABLE IF EXISTS `promotions`;
CREATE TABLE IF NOT EXISTS `promotions` (
  `idAnnonce` smallint(5) unsigned NOT NULL,
  `libelle` varchar(30) NOT NULL,
  `reduc` decimal(3,0) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vider la table avant d'insérer `promotions`
--

TRUNCATE TABLE `promotions`;
--
-- Contenu de la table `promotions`
--

INSERT INTO `promotions` (`idAnnonce`, `libelle`, `reduc`) VALUES
(1, 'Superpromo', 40),
(2, 'titou promo', 10),
(7, 'Promo d''hiver', 35),
(8, 'Pas cher pas cher', 60);

-- --------------------------------------------------------

--
-- Structure de la table `reservationA`
--

DROP TABLE IF EXISTS `reservationA`;
CREATE TABLE IF NOT EXISTS `reservationA` (
  `idProgramme` smallint(5) unsigned NOT NULL,
  `idSceanceA` smallint(5) unsigned NOT NULL,
  `heureDebut` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `heureFin` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nbPacesReservees` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vider la table avant d'insérer `reservationA`
--

TRUNCATE TABLE `reservationA`;
-- --------------------------------------------------------

--
-- Structure de la table `reservationH`
--

DROP TABLE IF EXISTS `reservationH`;
CREATE TABLE IF NOT EXISTS `reservationH` (
  `idProgramme` smallint(5) unsigned NOT NULL,
  `idSceanceH` smallint(5) unsigned NOT NULL,
  `dateDebut` date NOT NULL,
  `dateFin` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vider la table avant d'insérer `reservationH`
--

TRUNCATE TABLE `reservationH`;
-- --------------------------------------------------------

--
-- Structure de la table `restaurations`
--

DROP TABLE IF EXISTS `restaurations`;
CREATE TABLE IF NOT EXISTS `restaurations` (
  `id` smallint(5) unsigned NOT NULL,
  `nbEtoiles` tinyint(4) DEFAULT NULL,
  `lieu` varchar(10000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vider la table avant d'insérer `restaurations`
--

TRUNCATE TABLE `restaurations`;
--
-- Contenu de la table `restaurations`
--

INSERT INTO `restaurations` (`id`, `nbEtoiles`, `lieu`) VALUES
(3, 0, 'Allée de la Shizu'),
(4, 3, 'Allée de la Juju'),
(5, 0, 'Allée de Devil'),
(11, 5, 'Allée du Coli'),
(28, 0, 'Ici et pas là bas !');

-- --------------------------------------------------------

--
-- Structure de la table `seancesA`
--

DROP TABLE IF EXISTS `seancesA`;
CREATE TABLE IF NOT EXISTS `seancesA` (
  `id` smallint(5) unsigned NOT NULL,
  `idAnnonce` smallint(5) unsigned NOT NULL,
  `heureDebut` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `heureFin` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lieuDebut` varchar(500) NOT NULL,
  `lieuFin` varchar(500) NOT NULL,
  `dureeMin` time NOT NULL,
  `dureeMax` time NOT NULL,
  `nbPlaces` tinyint(4) NOT NULL,
  `prix` double unsigned NOT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vider la table avant d'insérer `seancesA`
--

TRUNCATE TABLE `seancesA`;
--
-- Contenu de la table `seancesA`
--

INSERT INTO `seancesA` (`id`, `idAnnonce`, `heureDebut`, `heureFin`, `lieuDebut`, `lieuFin`, `dureeMin`, `dureeMax`, `nbPlaces`, `prix`, `description`) VALUES
(0, 2, '2014-12-20 08:00:00', '2014-12-29 11:00:00', 'rue du depart', 'rue de l arrivee', '02:00:00', '02:00:00', 10, 15, 'Avec un gentil moniteur'),
(1, 3, '2014-12-24 18:31:05', '2014-12-18 23:00:00', 'g', 'sdf', '04:00:00', '06:00:00', 10, 10, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `seancesH`
--

DROP TABLE IF EXISTS `seancesH`;
CREATE TABLE IF NOT EXISTS `seancesH` (
  `id` smallint(5) unsigned NOT NULL,
  `idHeberg` smallint(5) unsigned NOT NULL,
  `dateDebut` date NOT NULL,
  `dateFin` date NOT NULL,
  `dureeMax` int(10) NOT NULL,
  `dureeMin` int(10) NOT NULL,
  `prix` double unsigned NOT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vider la table avant d'insérer `seancesH`
--

TRUNCATE TABLE `seancesH`;
--
-- Contenu de la table `seancesH`
--

INSERT INTO `seancesH` (`id`, `idHeberg`, `dateDebut`, `dateFin`, `dureeMax`, `dureeMin`, `prix`, `description`) VALUES
(0, 10, '2014-12-18', '2014-12-24', 1, 3, 20, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sports`
--

DROP TABLE IF EXISTS `sports`;
CREATE TABLE IF NOT EXISTS `sports` (
  `id` smallint(5) unsigned NOT NULL,
  `niveau` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vider la table avant d'insérer `sports`
--

TRUNCATE TABLE `sports`;
--
-- Contenu de la table `sports`
--

INSERT INTO `sports` (`id`, `niveau`) VALUES
(0, 'debutant'),
(1, 'confirme'),
(2, 'pourTous'),
(17, 'confirme'),
(18, 'confirme'),
(23, 'expert'),
(26, 'debutant'),
(27, 'pourTous'),
(30, 'expert');

-- --------------------------------------------------------

--
-- Structure de la table `vacanciers`
--

DROP TABLE IF EXISTS `vacanciers`;
CREATE TABLE IF NOT EXISTS `vacanciers` (
  `id` smallint(5) unsigned NOT NULL,
  `dateDeNaissance` date NOT NULL,
  `valide` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vider la table avant d'insérer `vacanciers`
--

TRUNCATE TABLE `vacanciers`;
--
-- Contenu de la table `vacanciers`
--

INSERT INTO `vacanciers` (`id`, `dateDeNaissance`, `valide`) VALUES
(0, '1995-12-31', 0),
(1, '1995-12-30', 0),
(5, '1995-12-29', 0),
(6, '1995-12-28', 0),
(8, '1995-12-27', 0),
(10, '1995-12-26', 0),
(11, '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `V_prixMin`
--
DROP VIEW IF EXISTS `V_prixMin`;
CREATE TABLE IF NOT EXISTS `V_prixMin` (
`idAnnonce` smallint(6) unsigned
,`prix` double unsigned
);
-- --------------------------------------------------------

--
-- Structure de la vue `V_prixMin`
--
DROP TABLE IF EXISTS `V_prixMin`;

CREATE ALGORITHM=UNDEFINED DEFINER=`projet`@`localhost` SQL SECURITY DEFINER VIEW `V_prixMin` AS select `seancesA`.`idAnnonce` AS `idAnnonce`,min(`seancesA`.`prix`) AS `prix` from `seancesA` where ((not(`seancesA`.`idAnnonce` in (select `promotions`.`idAnnonce` from `promotions`))) and (now() < `seancesA`.`heureDebut`)) group by `seancesA`.`idAnnonce` union select `seancesA`.`idAnnonce` AS `idAnnonce`,((min(`seancesA`.`prix`) * (100 - `promotions`.`reduc`)) / 100) AS `prix` from (`seancesA` join `promotions`) where ((`seancesA`.`idAnnonce` = `promotions`.`idAnnonce`) and (now() < `seancesA`.`heureDebut`)) group by `seancesA`.`idAnnonce` union select `seancesH`.`idHeberg` AS `idAnnonce`,min(`seancesH`.`prix`) AS `prix` from `seancesH` where ((not(`seancesH`.`idHeberg` in (select `promotions`.`idAnnonce` from `promotions`))) and (curdate() < `seancesH`.`dateDebut`)) group by `seancesH`.`idHeberg` union select `seancesH`.`idHeberg` AS `idAnnonce`,((min(`seancesH`.`prix`) * (100 - `promotions`.`reduc`)) / 100) AS `prix` from (`seancesH` join `promotions`) where ((`seancesH`.`idHeberg` = `promotions`.`idAnnonce`) and (curdate() < `seancesH`.`dateDebut`)) group by `seancesH`.`idHeberg`;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `amis`
--
ALTER TABLE `amis`
 ADD PRIMARY KEY (`id1`,`id2`), ADD KEY `id2` (`id2`);

--
-- Index pour la table `annonces`
--
ALTER TABLE `annonces`
 ADD PRIMARY KEY (`id`), ADD KEY `idPrestataires` (`idPrestataires`);

--
-- Index pour la table `commentaires`
--
ALTER TABLE `commentaires`
 ADD PRIMARY KEY (`id`), ADD KEY `idAnnonce` (`idAnnonce`), ADD KEY `idCompte` (`idCompte`);

--
-- Index pour la table `comptes`
--
ALTER TABLE `comptes`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `email` (`email`,`pseudo`);

--
-- Index pour la table `hebergements`
--
ALTER TABLE `hebergements`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `packs`
--
ALTER TABLE `packs`
 ADD PRIMARY KEY (`idProgramme`);

--
-- Index pour la table `paniers`
--
ALTER TABLE `paniers`
 ADD PRIMARY KEY (`idCompte`,`idAnnonce`), ADD KEY `idAnnonce` (`idAnnonce`);

--
-- Index pour la table `photos`
--
ALTER TABLE `photos`
 ADD PRIMARY KEY (`id`), ADD KEY `idCompte` (`idCompte`), ADD KEY `idAnnonce` (`idAnnonce`);

--
-- Index pour la table `prestataires`
--
ALTER TABLE `prestataires`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `programmes`
--
ALTER TABLE `programmes`
 ADD PRIMARY KEY (`id`), ADD KEY `idCompte` (`idCompte`);

--
-- Index pour la table `promotions`
--
ALTER TABLE `promotions`
 ADD PRIMARY KEY (`idAnnonce`);

--
-- Index pour la table `reservationA`
--
ALTER TABLE `reservationA`
 ADD PRIMARY KEY (`idProgramme`,`idSceanceA`), ADD KEY `idSceanceA` (`idSceanceA`);

--
-- Index pour la table `reservationH`
--
ALTER TABLE `reservationH`
 ADD PRIMARY KEY (`idProgramme`,`idSceanceH`), ADD KEY `idSceanceH` (`idSceanceH`);

--
-- Index pour la table `restaurations`
--
ALTER TABLE `restaurations`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `seancesA`
--
ALTER TABLE `seancesA`
 ADD PRIMARY KEY (`id`), ADD KEY `idAct` (`idAnnonce`), ADD KEY `idAnnonce` (`idAnnonce`);

--
-- Index pour la table `seancesH`
--
ALTER TABLE `seancesH`
 ADD PRIMARY KEY (`id`), ADD KEY `idHeberg` (`idHeberg`);

--
-- Index pour la table `sports`
--
ALTER TABLE `sports`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `vacanciers`
--
ALTER TABLE `vacanciers`
 ADD PRIMARY KEY (`id`);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `amis`
--
ALTER TABLE `amis`
ADD CONSTRAINT `amis_ibfk_1` FOREIGN KEY (`id1`) REFERENCES `comptes` (`id`),
ADD CONSTRAINT `amis_ibfk_2` FOREIGN KEY (`id2`) REFERENCES `comptes` (`id`);

--
-- Contraintes pour la table `annonces`
--
ALTER TABLE `annonces`
ADD CONSTRAINT `annonces_ibfk_1` FOREIGN KEY (`idPrestataires`) REFERENCES `prestataires` (`id`);

--
-- Contraintes pour la table `commentaires`
--
ALTER TABLE `commentaires`
ADD CONSTRAINT `commentaires_ibfk_1` FOREIGN KEY (`idCompte`) REFERENCES `vacanciers` (`id`),
ADD CONSTRAINT `commentaires_ibfk_2` FOREIGN KEY (`idAnnonce`) REFERENCES `annonces` (`id`);

--
-- Contraintes pour la table `hebergements`
--
ALTER TABLE `hebergements`
ADD CONSTRAINT `hebergements_ibfk_1` FOREIGN KEY (`id`) REFERENCES `annonces` (`id`);

--
-- Contraintes pour la table `packs`
--
ALTER TABLE `packs`
ADD CONSTRAINT `packs_ibfk_1` FOREIGN KEY (`idProgramme`) REFERENCES `programmes` (`id`);

--
-- Contraintes pour la table `paniers`
--
ALTER TABLE `paniers`
ADD CONSTRAINT `paniers_ibfk_1` FOREIGN KEY (`idCompte`) REFERENCES `vacanciers` (`id`),
ADD CONSTRAINT `paniers_ibfk_2` FOREIGN KEY (`idAnnonce`) REFERENCES `annonces` (`id`);

--
-- Contraintes pour la table `photos`
--
ALTER TABLE `photos`
ADD CONSTRAINT `photos_ibfk_1` FOREIGN KEY (`idCompte`) REFERENCES `comptes` (`id`),
ADD CONSTRAINT `photos_ibfk_2` FOREIGN KEY (`idAnnonce`) REFERENCES `annonces` (`id`);

--
-- Contraintes pour la table `prestataires`
--
ALTER TABLE `prestataires`
ADD CONSTRAINT `prestataires_ibfk_1` FOREIGN KEY (`id`) REFERENCES `comptes` (`id`);

--
-- Contraintes pour la table `programmes`
--
ALTER TABLE `programmes`
ADD CONSTRAINT `programmes_ibfk_1` FOREIGN KEY (`idCompte`) REFERENCES `vacanciers` (`id`);

--
-- Contraintes pour la table `promotions`
--
ALTER TABLE `promotions`
ADD CONSTRAINT `promotions_ibfk_1` FOREIGN KEY (`idAnnonce`) REFERENCES `annonces` (`id`);

--
-- Contraintes pour la table `reservationA`
--
ALTER TABLE `reservationA`
ADD CONSTRAINT `reservationA_ibfk_1` FOREIGN KEY (`idProgramme`) REFERENCES `programmes` (`id`),
ADD CONSTRAINT `reservationA_ibfk_2` FOREIGN KEY (`idSceanceA`) REFERENCES `seancesA` (`id`);

--
-- Contraintes pour la table `reservationH`
--
ALTER TABLE `reservationH`
ADD CONSTRAINT `reservationH_ibfk_1` FOREIGN KEY (`idProgramme`) REFERENCES `programmes` (`id`),
ADD CONSTRAINT `reservationH_ibfk_2` FOREIGN KEY (`idSceanceH`) REFERENCES `seancesH` (`id`);

--
-- Contraintes pour la table `restaurations`
--
ALTER TABLE `restaurations`
ADD CONSTRAINT `restaurations_ibfk_1` FOREIGN KEY (`id`) REFERENCES `annonces` (`id`);

--
-- Contraintes pour la table `seancesA`
--
ALTER TABLE `seancesA`
ADD CONSTRAINT `seancesA_ibfk_1` FOREIGN KEY (`idAnnonce`) REFERENCES `annonces` (`id`);

--
-- Contraintes pour la table `seancesH`
--
ALTER TABLE `seancesH`
ADD CONSTRAINT `seancesH_ibfk_1` FOREIGN KEY (`idHeberg`) REFERENCES `hebergements` (`id`);

--
-- Contraintes pour la table `sports`
--
ALTER TABLE `sports`
ADD CONSTRAINT `sports_ibfk_1` FOREIGN KEY (`id`) REFERENCES `annonces` (`id`);

--
-- Contraintes pour la table `vacanciers`
--
ALTER TABLE `vacanciers`
ADD CONSTRAINT `vacanciers_ibfk_1` FOREIGN KEY (`id`) REFERENCES `comptes` (`id`);

CREATE USER 'projet'@'localhost' IDENTIFIED BY '***';
GRANT USAGE ON *.* TO 'projet'@'localhost' IDENTIFIED BY '***'
	WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
GRANT ALL PRIVILEGES ON `projet`.* TO 'projet'@'localhost';

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
