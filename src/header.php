

<span id="langue">
    Langue :
    <span title="Français">
        <a lang="fr" href="#">
            <img src="img/fra.png" alt="Français" />
        </a>
    </span>
    <span title="English">
        <a lang="en" href="#">
            <img src="img/uk.png" alt="English" />
        </a>
    </span>
</span>

<span id="login">
	<?php
		if(isset($_COOKIE['pseudo']) && isset($_COOKIE['mdp_util']) && isset($_COOKIE['id_util'])){	
	?>
    <a href="?q=moncompte">
        Mon Compte
    </a>
    <a href="?q=deconnexion">
        Déconnexion
    </a>
    <a href="?q=monpanier">
        <img src="img/panier.png" alt="Votre Panier" />
    </a>
<?php	
	}else{
?>	
	<a href="?q=facebook">
        <img src="img/fb.png" alt="Facebook" />
    </a>
    <a href="?q=inscription">
        Inscription
    </a>
    <a href="?q=connexion">
        Connexion
    </a>
    <a href="?q=monpanier">
        <img src="img/panier.png" alt="Votre Panier" />
    </a>

<?php
	}
?>
</span>

<h1><span>Travel Voyage</span>Le meilleur des vacances</h1>