<?php
//upload($compte,$annonce) -> retourne 0 si echec, 1 si ok
//deleteImage($idImage)
$tailleMaxImage=5000000; //10 mega octet max
$TailleMiniature=300;
$localisationGrande="/var/www/clients/client1/web3/web/projet/commun2/img/imgAnnonce/reel/";
$localisationPetite="/var/www/clients/client1/web3/web/projet/commun2/img/imgAnnonce/miniature/";
$extensionsAutorisees = array("jpeg","JPEG","jpg","JPG","gif","GIF","png","PNG");

//retourne 0 si echec, 1 si ok
function upload($compte,$annonce){
	global $tailleMaxImage;
	global $TailleMiniature;
	global $localisationGrande;
	global $localisationPetite;
	global $extensionsAutorisees;

	if (isset($_FILES['monfichier']) && isset($_POST['MAX_FILE_SIZE']) && $_POST['MAX_FILE_SIZE']<=$tailleMaxImage){
		$nomOrigine = $_FILES['monfichier']['name'];
		$elementsChemin = pathinfo($nomOrigine);
		$extensionFichier = $elementsChemin['extension'];
		
		if (in_array($extensionFichier, $extensionsAutorisees)) {
			$nom=insertBD($compte,$annonce,$extensionFichier);
			if ($nom>=0){
				// Copie dans le repertoire du script avec un nom incluant l'heure a la seconde pres 
				$nomDestination = $nom.".".$extensionFichier;
				move_uploaded_file($_FILES["monfichier"]["tmp_name"],$localisationGrande.$nomDestination);
				if (imagethumb($localisationGrande.$nomDestination,$localisationPetite.$nomDestination,$TailleMiniature)){
					return 1;
				} else {
					deleteImage($nom);
					return 0;
				}				
			} else {
				return 0;
			}
		} else {  
			return 0;
		}
	} else {
		return 0;
	}
}

function deleteImage($idImage){
	global $localisationGrande;
	global $localisationPetite;
	
	try{
		$bdd = new PDO('mysql:host=localhost;dbname=projet;charset=utf8', 'projet', 'tejorp');
		$bdd -> exec("set names utf8");
	}catch(Exception $e){
		die('Erreur : '.$e->getMessage());
	}
	
	$sql=$bdd->query("SELECT extension FROM photos WHERE id=$idImage")->fetch();
	if (file_exists($localisationGrande.$idImage.'.'.$sql['extension'])){
		unlink($localisationGrande.$idImage.'.'.$sql['extension']);
	}
	if (file_exists($localisationPetite.$idImage.'.'.$sql['extension'])){
		unlink($localisationPetite.$idImage.'.'.$sql['extension']); 
	}
	$bdd->exec("DELETE FROM photos WHERE id=$idImage");
}





//Utilisation interne

//retourne le numero de l'image
function insertBD($compte,$annonce,$extension){
	try{
		$bdd = new PDO('mysql:host=localhost;dbname=projet;charset=utf8', 'projet', 'tejorp');
		$bdd -> exec("set names utf8");
	}catch(Exception $e){
		die('Erreur : '.$e->getMessage());
	}
	
	$donnees = $bdd->query('SELECT max(id) FROM `photos`')->fetch();
    $id=$donnees['max(id)']+1;
	$bdd->exec("INSERT INTO `photos`(`id`, `idCompte`, `idAnnonce`, `extension`) VALUES ($id,$compte,$annonce,'$extension')");
	return $id;
}

// Merci http://code.seebz.net/p/imagethumb/
function imagethumb( $image_src , $image_dest = NULL , $max_size = 100, $expand = FALSE, $square = FALSE ) {
	if( !file_exists($image_src) ) return FALSE;

	// R�cup�re les infos de l'image
	$fileinfo = getimagesize($image_src);
	if( !$fileinfo ) return FALSE;

	$width     = $fileinfo[0];
	$height    = $fileinfo[1];
	$type_mime = $fileinfo['mime'];
	$type      = str_replace('image/', '', $type_mime);

	if( !$expand && max($width, $height)<=$max_size && (!$square || ($square && $width==$height) ) )
	{
		// L'image est plus petite que max_size
		if($image_dest)
		{
			return copy($image_src, $image_dest);
		}
		else
		{
			header('Content-Type: '. $type_mime);
			return (boolean) readfile($image_src);
		}
	}

	// Calcule les nouvelles dimensions
	$ratio = $width / $height;

	if( $square )
	{
		$new_width = $new_height = $max_size;

		if( $ratio > 1 )
		{
			// Paysage
			$src_y = 0;
			$src_x = round( ($width - $height) / 2 );

			$src_w = $src_h = $height;
		}
		else
		{
			// Portrait
			$src_x = 0;
			$src_y = round( ($height - $width) / 2 );

			$src_w = $src_h = $width;
		}
	}
	else
	{
		$src_x = $src_y = 0;
		$src_w = $width;
		$src_h = $height;

		if ( $ratio > 1 )
		{
			// Paysage
			$new_width  = $max_size;
			$new_height = round( $max_size / $ratio );
		}
		else
		{
			// Portrait
			$new_height = $max_size;
			$new_width  = round( $max_size * $ratio );
		}
	}

	// Ouvre l'image originale
	$func = 'imagecreatefrom' . $type;
	if( !function_exists($func) ) return FALSE;

	$image_src = $func($image_src);
	$new_image = imagecreatetruecolor($new_width,$new_height);

	// Gestion de la transparence pour les png
	if( $type=='png' )
	{
		imagealphablending($new_image,false);
		if( function_exists('imagesavealpha') )
			imagesavealpha($new_image,true);
	}

	// Gestion de la transparence pour les gif
	elseif( $type=='gif' && imagecolortransparent($image_src)>=0 )
	{
		$transparent_index = imagecolortransparent($image_src);
		$transparent_color = imagecolorsforindex($image_src, $transparent_index);
		$transparent_index = imagecolorallocate($new_image, $transparent_color['red'], $transparent_color['green'], $transparent_color['blue']);
		imagefill($new_image, 0, 0, $transparent_index);
		imagecolortransparent($new_image, $transparent_index);
	}

	// Redimensionnement de l'image
	imagecopyresampled(
		$new_image, $image_src,
		0, 0, $src_x, $src_y,
		$new_width, $new_height, $src_w, $src_h
	);

	// Enregistrement de l'image
	$func = 'image'. $type;
	if($image_dest)
	{
		$func($new_image, $image_dest);
	}
	else
	{
		header('Content-Type: '. $type_mime);
		$func($new_image);
	}

	// Lib�ration de la m�moire
	imagedestroy($new_image); 

	return TRUE;
}

?>