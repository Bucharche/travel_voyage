<script type="text/javascript">
	function sizePlusPlus(li){
		li.parentNode.parentNode.rowSpan += 1;
		li.parentNode.style.height= (li.parentNode.offsetHeight+33)+"px";
		
		x = getCellX(li.parentNode.parentNode)+1;
		y = getCellY(li.parentNode.parentNode)+li.parentNode.parentNode.rowSpan;
		
		getCell(x,y).parentNode.removeChild(getCell(x,y));
	}
	
	function getCell(x, y){
		return document.getElementById("planning").rows[y-1].cells.item(x-1);
	}
	
	function getCellX(cell){
		return cell.cellIndex;
	}
	
	function getCellY(cell){
		return cell.parentNode.rowIndex;
	}
</script>

<ul class="dropper">
	<?php
		/*$list = array();
		if(isset($_COOKIE["panier"])){
			$list = explode('/', $_COOKIE["panier"]);
		}*/
		
		try{
			$bdd = new PDO('mysql:host=localhost;dbname=projet;charset=utf8', 'projet', 'tejorp');
		}catch(Exception $e){
			die('Erreur : '.$e->getMessage());
		}
		
		require_once("/var/www/clients/client1/web3/web/projet/commun2/compte/util.php");
		if (estConnecte()==1){
			$requete=$bdd->query("SELECT idAnnonce from paniers where idCompte=$_COOKIE[id_util]");
			$tmp="";
			while ($res=$requete->fetch()){
				$tmp.=$res['idAnnonce']."/";
			}
			if (isset($tmp{1})) {
				$tmp=substr($tmp,0,-1);
				$list=explode("/",$tmp);
			}
		} else if (estConnecte()==0 && isset($_COOKIE['panier'])) {
			$list=explode("/",$_COOKIE['panier']);
		} 
		
		if (isset($list)){		
			foreach($list as $idAnnonce){
				$nom = $bdd->query('SELECT nom FROM `annonces` WHERE id='.$idAnnonce);
				$nom = $nom->fetch();
				echo("<li class=\"draggable\">".$nom["nom"]."<a href=\"#\">-</a><div class=\"handlePlus\" onclick=\"sizePlusPlus(this);\">▼</div><div class=\"handleMoins\" onclick=\"sizeMoinsMoins(this);\">▲</div></li>");
			}
		}
	?>
</ul>
<table id="planning">
	<tr>
		<th></th>
		<th>Lundi</th>
		<th>Mardi</th>
		<th>Mercredi</th>
		<th>Jeudi</th>
		<th>Vendredi</th>
		<th>Samedi</th>
		<th>Dimanche</th>
	</tr>
	<?php
		for($i=0; $i<18; $i++){
			echo("<tr>");
			echo("\t\t<th>".(6+$i)."h</th>");
			for($j=0; $j<7; $j++){
				echo("\t\t<td class=\"dropper\"></td>\n");
			}
			echo("</tr>");
		}
	?>
</table>
<!--<div id="lol"></div>-->