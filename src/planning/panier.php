<?php
require_once("compte/util.php");

try{
	$bdd = new PDO('mysql:host=localhost;dbname=projet;charset=utf8', 'projet', 'tejorp');
}catch(Exception $e){
	die('Erreur : '.$e->getMessage());
}

$estConnecte=estConnecte();
if ($estConnecte==1){
	$requete=$bdd->query("SELECT idAnnonce from paniers where idCompte=$_COOKIE[id_util]");
	$panier="";
	while ($res=$requete->fetch()){
		$panier.=$res['idAnnonce']."/";
	}
	$panier=substr($panier,0,-1);
} else if ($estConnecte==0) {
	if (isset($_COOKIE['panier'])){
		$panier=$_COOKIE['panier'];
	} else {
		$panier="";
	}
} else {
	$panier="-1";
}
if (isset($_COOKIE['panier'])){$cookiePanier=$_COOKIE['panier'];}else{$cookiePanier="";}
if (isset($_GET['champ'])){$champ=$_GET['champ'];} else {$champ='nomAnnonce';}
if (isset($_GET['ordre'])){$ordre=$_GET['ordre'];} else {$ordre='asc';}

if (isset($_POST['supprimer'])){
	foreach($_POST as $key => $value){
		$tab=explode("_",$key);
		if ($tab[0]=="sup"){
			if ($estConnecte==1){
				$bdd->exec("DELETE FROM paniers WHERE idCompte=$_COOKIE[id_util] AND idAnnonce=$tab[1]");
			} else if ($estConnecte==0) {
				$tmp="";
				foreach(explode("/",$cookiePanier) as $value){
					if ($value!=$tab[1]){
						$tmp.=$value.'/';
					}
				}
				$cookiePanier=substr($tmp,0,-1);
			}		
		}
	}
	if ($estConnecte==0){
		setcookie('panier',$cookiePanier,(time()+ 365*24*3600),'/');
	}
} 
?>

<div>
<?php
if ($panier=="-1"){
	echo "<h2>Vous n'avez pas le droit d'etre sur cette page.</h2>";
} else if ($panier==""){
	echo "<h2>Votre panier est vide.</h2>";
} else {
	$paremetreURL="?";
	foreach ($_GET as $key => $value){
		$paremetreURL.=$key.'='.$value.'&';
	}
	$paremetreURL=substr($paremetreURL,0,-1);
	echo "<form  action=\"$paremetreURL\" method=\"POST\">\n";
	
	?>
	<table>
	<caption>Vote panier : </caption>
	<tr>
		<th>Nom <a href="?q=monpanier&champ=nomAnnonce&$ordre=asc">^</a> <a href="?q=monpanier&champ=nomAnnonce&ordre=desc">v</a></th>
		<th>Description <a href="?q=monpanier&champ=description&$ordre=asc">^</a> <a href="?q=monpanier&champ=description&ordre=desc">v</a></th>
		<th>Prix de départ <a href="?q=monpanier&champ=prix&$ordre=asc">^</a> <a href="?q=monpanier&champ=prix&ordre=desc">v</a></th>
		<th>Moyenne des notes <a href="?q=monpanier&champ=moyenne&$ordre=asc">^</a> <a href="?q=monpanier&champ=moyenne&ordre=desc">v</a></th> 
		<th>Matériel nécessaire <a href="?q=monpanier&champ=materiel&$ordre=asc">^</a> <a href="?q=monpanier&champ=materiel&ordre=desc">v</a></th>
		<th>Horaire libre <a href="?q=monpanier&champ=horaireLibre&$ordre=asc">^</a> <a href="?q=monpanier&champ=horaireLibre&ordre=desc">v</a></th>		
		<th>Prestataire <a href="?q=monpanier&champ=pseudo&$ordre=ASC">^</a> <a href="?q=monpanier&champ=pseudo&ordre=desc">v</a></th>
		<th>Type de service <a href="?q=monpanier&champ=typeService&$ordre=asc">^</a> <a href="?q=monpanier&champ=typeService&ordre=desc">v</a></th>
		<th>Sous-type <a href="?q=monpanier&champ=sousType&$ordre=asc">^</a> <a href="?q=monpanier&champ=sousType&ordre=desc">v</a></th>	
		<th>Supprimer</th>
	</tr> 
	<?php
	$panier=str_replace('/',',',$panier);
	$resultat=$bdd->query("SELECT annonces.id as idAnnonce, annonces.nom as nomAnnonce, annonces.description, materiel, horaireLibre, typeService, sousType, c.id as idPrestataire, pseudo, avg(note) as moyenne, prix 
							FROM annonces LEFT JOIN commentaires ON annonces.id=commentaires.idAnnonce  LEFT JOIN V_prixMin ON annonces.id=V_prixMin.idAnnonce,comptes c 
							WHERE annonces.id IN ($panier) AND annonces.idPrestataires=c.id 
							GROUP BY annonces.id 
							ORDER BY $champ $ordre, annonces.nom ASC");
	while ($req=$resultat->fetch()){
		echo "<tr>\n";
		if (!isset($_POST["sup_$req[idAnnonce]"])){
			echo "<td><a href=\"?q=annonce&id=$req[idAnnonce]\">$req[nomAnnonce]</a></td>\n";
			echo "<td><p>$req[description]</p></td>\n";
			if ($req['prix']!=null){
				echo "<td>$req[prix]</td>\n";
			} else {
				echo "<td>Pas de séance</td>\n";
			}
			if ($req['moyenne']!=null){
				echo "<td>$req[moyenne]</td>\n";
			} else {
				echo "<td>Pas de note</td>\n";
			}
			if($req['materiel']!=null){
				echo "<td>$req[materiel]</td>\n";
			} else {
				echo "<td>Pas de matériel</td>\n";
			}
			if($req['horaireLibre']==1){
				echo "<td>Oui</td>\n";
			} else {
				echo "<td>Non</td>\n";
			}
			echo "<td><a href=\"?q=prestataire&id=$req[idPrestataire]\">$req[pseudo]</a></td>\n";
			echo "<td>$req[typeService]</td>\n";
			echo "<td>$req[sousType]</td>\n";
			echo "<td><input type=\"checkbox\" id=\"sup_$req[idAnnonce]\" name=\"sup_$req[idAnnonce]\"></td>\n";
		}
		echo "</tr>\n";
	}
	echo "</table>\n";
	echo "<input type=\"submit\" value=\"Supprimer la sélection\" name=\"supprimer\">\n";
	echo "</form>\n";
}

?>
</div>
