<!-- 
	Ces fichiers sont déjà dans un 
	<body>
		<div id="contact">
	Pas besoin de les "encadrer" d'un cadre quelconque.
-->

<h3>Travel Voyage Corporation</h3>

<h4> Salut à vous ! </h4>

<p> Nous vous souhaitons la bienvenue sur notre site Travel-Voyage,
 le meilleur site en ligne de gestion de ses vacances à la montagne ! 
</p>

<p> Ce site n'aurait jamais pu voir le jour sans Yvan Bourrut, Thomas Jalabert, Juliette Pelletier, Céline Hernandez, Rémi Pontonnier et Nicolas Violette ! </p>

<p>Vous qui êtes ébahis à la vue de notre application, sachez que vous pouvez nous contacter pour plus d'informations ici : <a href="mailto:travel.voyage@outlook.com">travel.voyage@outlook.com </a> 
<br/><br/>
Merci et bonne visite !</p>

<div>
<p>Dans le cadre du projet M3301</p>
<p>Université Pierre-Mendès-France IUT2</p>
<p>© 2014-2015, tous droits réservés</p>
</div>



<!-- 
	Et donc après il y a tous les
		</div>
	</body>
	nécessaires.
-->