<!-- 
	Ces fichiers sont déjà dans un 
	<body>
		<div id="faq">
	Pas besoin de les "encadrer" d'un cadre quelconque.
-->



<h3> Foire aux questions ! </h3>

<h4> Pourquoi ce site ? </h4>

<p> Ce site a été créé dans le cadre d'un enseignement de développement web à l'Université Pierre-Mendès-France (IUT2) </p>

<h4> Est-ce que l'on peut vraiment réserver ses vacances sur ce site ? </h4>

<p> Non. </p> 

<h4> J'ai une suggestion, j'ai remarqué un bug ou une faille, comment je peux vous joindre ? </h4>

<p> L'onglet contact est fait pour ça. </p> 

<h4> Qui êtes-vous ? </h4>

<p> Idem. </p> 

<h4> Je ne peux pas me connecter, est-ce normal ? </h4>

<p> La réponse est non ! Allez dans les paramètres de votre navigateur et acceptez nos cookies. Si ça ne fonctionne toujours pas contactez-nous ! (Rubrique Pratique -> Contact)</p> 

<h4> J'ai oublié mon mot de passe, comment puis-je récupérer mon compte ? </h4>

<p> Hum... Quel compte ?  </p>

<h4> Le site ne s'affiche pas correctement, est-ce normal ? </h4>

<p> Il peux y avoir plusieurs réponses à cette question : 
<br/> Déjà, si vous utilisez internet explorer c'est mal parti...
<br/> Si en utilisant firefox certaines parties du site ne fonctionnent pas essayez d'activer le javascript. 
 C'est assez facile, il faut aller dans les paramètres de votre navigateur internet. 
<br/> Si après tout ça, ça ne marche toujours pas, contactez-nous ! (Rubrique Pratique -> Contact)</p> 











<!-- 
	Et donc après il y a tous les
		</div>
	</body>
	nécessaires.
-->