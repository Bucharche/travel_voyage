<?php

if(isset($_POST['typeservice'])) {
	//la requete se construit comme cela : 
	//- construction du tableau $choix avec les valeurs passées par l'utilisateur
	//- construction du string $critere qui concatène les valeurs de $choix
	
	//typeservice et catégorie sont commun 
	$typeservice = $_POST['typeservice'];
	$categorie = $_POST['soustype'];

	//champs différents en fonction du type de service
	if ($typeservice == "activite") {
		$materiel_necessaire = $_POST['materiel_necessaire'];
		
		//si le matériel est fourni, on recherche ...where materiel = NULL;
		if ($materiel_necessaire == "oui") $materiel = "materiel <> '' ";
		else if ($materiel_necessaire == "jsp") $materiel = "materiel like '%%'";
		
		//sinon, le matériel n'est pas fourni, on recherche dans la bd ou materiel = quelquechose
		else $materiel = "materiel like '' ";
		
		$seance_libre = $_POST['seance_libre'];
		if ($seance_libre == "jsp") $seance_libre = "%%";
		
		if (isset($_POST['difficulte']) && $categorie == "sportive") {
			$difficulte_tab = $_POST['difficulte'];
			
			//Pour le champ "N'importe"
			if (in_array("jsp",$difficulte_tab)) $difficulte = "s.niveau like '%%'";
			else {
				$difficulte = "( ";
				$i=1;
				foreach ($difficulte_tab as $v) {
					$difficulte.= "s.niveau = '".$v."'";
					if ($i < count($difficulte_tab) ) $difficulte.=" OR ";
					$i++;
					
				}
				$difficulte.=" ) ";
			}
			$requete = "SELECT a.id FROM annonces a, sports s WHERE a.typeservice = '".$typeservice."' AND a.soustype = '".$categorie.
			"' AND a.horairelibre like '" .$seance_libre."' AND a.".$materiel." AND a.id = s.id AND ".$difficulte;
		}
		else {
			$requete = "SELECT id FROM annonces WHERE typeservice = '".$typeservice."' AND soustype = '".$categorie.
			"' AND horairelibre like '" .$seance_libre."' AND ".$materiel;
		}
	}
	
	else if ($typeservice == "hebergement") {
		$nb_places = $_POST['nb_places'];
		$nb_etoiles = $_POST['nb_etoiles'];
		$jointure = " AND a.id = h.id";
		$requete = "SELECT a.id FROM annonces a, hebergements h WHERE a.typeservice = '$typeservice' AND a.soustype = '$categorie' $jointure AND h.nbPlaces >= $nb_places AND h.nbEtoiles = '$nb_etoiles';";	
	}
	
	else if ($typeservice == "restauration") {
		$nb_etoiles = $_POST['nb_etoiles'];
		$jointure = " AND a.id = r.id";
		$requete = "SELECT a.id FROM annonces a, restaurations r WHERE typeservice = '".$typeservice."' AND soustype = '"
		.$categorie."'".$jointure." AND nbEtoiles = '".$nb_etoiles."'";	
	}

if (isset($typeservice)) {
		mysql_connect('localhost', 'projet', 'tejorp');
		mysql_select_db('projet');
		$query = mysql_query($requete) or die(mysql_error());
		$nb_resultats = mysql_num_rows($query);
		$id = "";
		while ($donnees = mysql_fetch_array($query)) {
			$id.=$donnees['id']."_";
		}
		$id=substr($id,0,-1); // on enleve le "_" de trop	
		echo "<meta http-equiv=\"refresh\" content=\"0; URL=?e=selectionnez&idAnnonce=$id\">";
		mysql_close();
	} else {
		echo "Votre recherche est vide, recommencez <br/>";
		echo "<a href=\"r=avancee.php\">Réessayez</a> avec autre chose.";
	}


//premiere fois sur la page => partie HTML
} else {
?>
<form method="post" action="?r=avancee">
		<p title="Sélectionnez le type d'annonce que vous proposez">
                    <label>Type : </label>
                    <select name="typeservice" id="typeservice" >
                        <option value="activite">Activité</option>
                        <option value="hebergement"> Hébergement</option>
                        <option value="restauration">Restauration</option>
                    </select>
        </p>
                <p title="Précisez la catégorie dans laquelle entre votre annonce" style="display:none;" id="sous_type">
                    
					<fieldset class="disabled" disabled id="gestion_a" style="display:none;">
						
						<label>Catégorie : </label>
						<select name="soustype" id="categorie" >
							<option value="culturelle" checked>Culturelle</option>
							<option value="loisir">Loisir</option>
							<option value="sportive">Sportive</option>
							<option value="autre_activite">Autre</option>
						</select>

						<p title="Indiquez si l'activité que vous recherchez nécessite du matériel">
							<label for="materiel">Matériel nécessaire :</label>
							<input type="radio" name="materiel_necessaire" id="materiel_necessaire" value="jsp" checked /> <label fol "libre_jsp">N'importe</label>
							<input type="radio" name="materiel_necessaire" id="materiel_necessaire" value="oui"/> <label for="libre_oui">Oui</label>
							<input type="radio" name="materiel_necessaire" id="materiel_necessaire" value="non"/> <label for="libre_non">Non</label>
						</p>
						
						<p title="Indiquez si l'activité a des horaires précis ou si vous pouvez venir à l'heure qu'il souhaite ">
							<label>Séance libre : </label>
							<input type="radio" name="seance_libre" id="libre_jsp" value="jsp" checked /> <label for="jsp">N'importe</label>
							<input type="radio" name="seance_libre" id="libre_oui" value="1" /> <label for="libre_oui">Oui</label>
							<input type="radio" name="seance_libre" id="libre_non" value="0" /> <label for="libre_non">Non</label>
							
						</p>
						
						<fieldset class="disabled" id="sport" style="display:none;" disabled>
							<p title="Indiquez une difficulté maximale">
								<label>Difficulté(s) voulue(s) : </label>
								<input type = "checkbox" name="difficulte[]" id="difficulte_jsp" value="jsp"/><label for="difficulte_jsp">N'importe</label>
								<input type = "checkbox" name="difficulte[]" id="difficulte_debutant" value="debutant"/><label for="difficulte_debutant">Debutant</label>
								<input type = "checkbox" name="difficulte[]" id="difficulte_confirme" value="confirme"/><label for="difficulte_confirme">Confirmé</label>
								<input type = "checkbox" name="difficulte[]" id="difficulte_expert" value="expert"/><label for="difficulte_expert">Expert</label>
								<input type = "checkbox" name="difficulte[]" id="difficulte_pourTous" value="pourTous"/><label for="difficulte_pourTous">Pour tous</label>
							</p>
						</fieldset>
				
					</fieldset>
					
					<fieldset class="disabled" disabled id="gestion_h" style="display:none;">
						<label>Catégorie : </label>
						<select name="soustype" id="hebergements" >
							<option value="auberge">Auberge de jeunesse</option>
							<option value="camping">Camping</option>
							<option value="chambres">Chambres d'hôtes</option>
							<option value="gite">Gîte</option>
							<option value="hotel">Hôtel</option>
							<option value="refuge">Refuge</option>
							<option value="autre_hebergement">Autre</option>
						</select>
						
						<p title="Entrez le nombre de places disponibles dans votre hébergement "> 
							<label>Nombre de places : </label>
							<input name = "nb_places" value="1" id="nb_places" type="number" min="1" max="50" required/>
						</p>
						
						<p title="Entrez le nombre d'étoiles de votre hébergement"> 
							<label>Nombre d'étoiles :</label>
							<input name = "nb_etoiles" value = "0" id="nb_etoiles" type="number" min="0" max="5"/>
						</p>
					</fieldset>
					
					<fieldset class="disabled" disabled id="gestion_r" style="display:none;">
						<label>Catégorie : </label>
						<select name="soustype" id="restauration" >
							<option value="bar">Bar</option>
							<option value="restaurant">Restaurant</option>
							<option value="snack">Snack</option>
							<option value="autre_restauration">Autre</option>
						</select>
						<p title="Entrez le nombre d'étoiles de votre établissement"> 
							<label>Nombre d'étoiles :</label>
							<input name = "nb_etoiles" value = "0" id="nb_etoiles" type="number" min="0" max="5"/>
						</p>
					</fieldset>
                </p>
                <input type="submit" value="Rechercher" />
</form>

        <script src="recherche/recherche_avancee.js">
        </script>
	
<?php
	}
?>