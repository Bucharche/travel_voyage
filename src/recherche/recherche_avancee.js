/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var list_sel = document.getElementById('typeservice');
list_sel.selectedIndex=0;

// pour savoir si c'est sport ou loisir etc qui a �t� s�lectionn�
var activite_sel = document.getElementById('categorie');

var list = document.getElementById('typeservice');

   // variables 
var sous_type, activites, hebergement,restauration, sport;

sous_type = document.getElementById('sous_type');
activites = document.getElementById('gestion_a');
hebergement = document.getElementById('gestion_h');
restauration = document.getElementById('gestion_r');
sport = document.getElementById('sport');

// affiche les activit�s au chargement de la page
window.onload = afficherActivite();

activite_sel.addEventListener('change', function() {
	// on r�cup�re la cat�gorie d'activit� : sport, culturelle etc
	
	if(activite_sel.value === "sportive"){
		afficherSport();
	} else cacherSport();

}, true);



list.addEventListener('change', function() {
    // on r�cup�re le type d'annonce : activit� ou h�bergement
    var selected = list.options[list.selectedIndex].value;

    // On affiche le contenu de l'�l�ment <option> cibl� par la propri�t� selectedIndex
 
    if (selected === 'activite') {
        afficherActivite();	
	}
	else if (selected === 'hebergement') {
        afficherHebergement();
    }
	else if (selected === 'restauration'){
		afficherRestauration();
	}
	else cacherTout();
	
}, true);

function afficherActivite() {
    // on affiche le paragraphe des sous-types
    sous_type.style.display = "block";

    // on affiche tout ce qui concerne les activit�s
    activites.style.display = "inline";
	activites.disabled = false;
    // on masque tout ce qui concerne les h�bergements et la restauration
    hebergement.style.display = "none";
	hebergement.disabled = true;
	restauration.style.display= "none";
	restauration.disabled = true;
}

function afficherHebergement() {
    // on masque tout ce qui concerne les activit�s et la restauration
    activites.style.display = "none";
	activites.disabled = true;
	restauration.style.display= "none";
	restauration.disabled = true;
	
    // on affiche le paragraphe des sous-types
    sous_type.style.display = "block";

	
    // on affiche tout ce qui concerne les h�bergements
    hebergement.style.display = "inline";
	hebergement.disabled = false;
}

function afficherRestauration() {
    // on masque tout ce qui concerne les activit�s et les h�bergements
    activites.style.display = "none";
	activites.disabled = true;
	hebergement.style.display = "none";
	hebergement.disabled = true;
	
    // on affiche le paragraphe des sous-types
    sous_type.style.display = "block";

    // on affiche tout ce qui concerne la restauration
    restauration.style.display = "inline";
	restauration.disabled = false;
}

function afficherSport(){
	sport.style.display = "block";
	sport.disabled=false;
}

function cacherSport(){
	sport.style.display = "none";
	sport.disabled=true;
}

function cacherTout() {
	restauration.style.display= "none";
	restauration.disabled = true;
    hebergement.style.display = "none";
	hebergement.disabled = true;
    activites.style.display = "none";
	activites.disabled = true;
	
    sous_type.style.display = "none";
}