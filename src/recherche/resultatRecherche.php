<?php
//La page a beasoin des id des annonces contenue dans idAnnonce et separer par _
include_once("compte/util.php");

if (isset($_GET['idAnnonce'])){
	$requete=$_GET['idAnnonce'];
	setcookie('recherche',$requete,(time()+ 365*24*3600),'/');	
} else if (isset($_COOKIE['recherche'])) {
	$requete=$_COOKIE['recherche'];
} else {
	$requete="";
}
if (isset($_COOKIE['panier'])){$cookiePanier=$_COOKIE['panier'];} else {$cookiePanier="";}
if (isset($_GET['nbParPage']) && $_GET['nbParPage']>=1){
	$nbParPage=$_GET['nbParPage'];
	setcookie('nbParPage',$nbParPage,(time()+ 365*24*3600),'/');	
} else if (isset($_COOKIE['nbParPage']) && $_COOKIE['nbParPage']>=1) {
	$nbParPage=$_COOKIE['nbParPage'];
} else {
	$nbParPage=10;
}
if (!isset($_GET["index"]) || $_GET["index"]<0){$index=0;} else {$index=$_GET["index"];}


$estConnecte=estConnecte(); 

echo "<div>";
try{
	$bdd = new PDO('mysql:host=localhost;dbname=projet;charset=utf8', 'projet', 'tejorp');
}catch(Exception $e){
	die('Erreur : '.$e->getMessage());
}

$id=explode("_",$requete);


if($id[0]==""){
	echo "<h3>Pas d'annonce</h3>";
	echo "<p>Nous n'avons trouvé aucune annonce pour votre requête </p>";
} else {
	$position=0;
	while($position<$index){
		$position++;
	}
	$indiceID=$position;
	while($position<($index+$nbParPage) && $position<count($id)){
		$cocher[$id[$position]]=panier($bdd,$id[$position]);
		$position++;
	}
	setcookie('panier',$cookiePanier,(time()+ 365*24*3600),'/');	

	echo "<h3>Annonces correspondantes : </h3>";
	echo "<p>Nous avons trouvé " . count($id);
	// on vérifie le nombre de résultats pour orthographier correctement. 
	if (count($id)>1) {
		echo ' annonces ';
	} else {
		echo ' annonce ';
	}
	echo "dans notre base de données.</p></div>";	
		
	echo("<nav>Page : ");
	$chaine="?";
	foreach($_GET as $key => $value){
		if($key!="index" && $key!="nbParPage" && $key!="idAnnonce"){
			$chaine.="$key=$value&";
		}
	}
	$chaine.="idAnnonce=$requete";
	if ($index>0){
		echo("<a href=\"$chaine&index=".($index-$nbParPage)."&nbParPage=$nbParPage\"> Précédente </a>");
	}
	for($nbPage=0; $nbPage*$nbParPage<count($id); $nbPage++){
		echo("<a href=\"$chaine&index=".($nbPage*$nbParPage)."&nbParPage=$nbParPage\">".($nbPage+1)." </a>");
	}
	if ($index<(count($id)-$nbParPage)){
		echo("<a href=\"$chaine&index=".($index+$nbParPage)."&nbParPage=$nbParPage\"> Suivante </a>");
	}
	

	echo "<form action=\"?\" method=\"GET\">";
	foreach($_GET as $key => $value){ //Permet de envoyer la page avec les meme valeur en GET
		if ($key!="nbParPage" && $key!="idAnnonce"){
			echo "<input id=_\"$key\" style=\"display : none;\" name=\"$key\" type=\"text\" value=$value />";
		}
	}
	echo "<input id=_\"idAnnonce\" style=\"display : none;\" name=\"idAnnonce\" type=\"text\" value=$requete />";
?>
	<label for="nbParPage">Nombre par page : </label>
	<input id="nbParPage" name="nbParPage" type="number" min="1" value="<?php echo($nbParPage); ?>" />
	<input type="submit" id="ok" value="Appliquer" />
<?php
	echo "</form>";


	echo("</nav>");

	$chaine="?";
	foreach($_GET as $key => $value){
		$chaine.=$key.'='.$value."&";
	}
	$chaine=substr($chaine,0,-1);
	echo "<form action=\"$chaine\" method=\"POST\">";
	
	while($indiceID<count($id) && $nbParPage>0 && $id[$indiceID]!=""){
		$reponse = $bdd->query("SELECT id,nom,description FROM annonces WHERE id=$id[$indiceID]");
		$donnees = $reponse->fetch();
		echo "<div>\n";
		if ($donnees!=null){
			echo "<h4>".$donnees['nom']."</h4>\n";
			$prix=$bdd->query("SELECT prix FROM V_prixMin WHERE idAnnonce=$donnees[id]")->fetch();
			if ($prix['prix']==null){
				echo "<p>Attention, il n'y pas de séance pour cette annonce.</p>\n";
			} else {
				echo "<p>A partir de $prix[prix] €</p>\n";
			}
			echo "<p>".$donnees['description']."</p>\n";
			echo "<p><a href=\"?q=annonce&id=$donnees[id]\">Plus d'informations</a></p>\n";
			if ($estConnecte==1 || $estConnecte==0){
				if ($cocher[$donnees['id']]==1){
					echo "<input type=\"checkbox\" checked id=\"$donnees[id]\" name=\"$donnees[id]\"><label for=\"$donnees[id]\">Ajouter au panier</label>\n";
				} else {
					echo "<input type=\"checkbox\" id=\"$donnees[id]\" name=\"$donnees[id]\"><label for=\"$donnees[id]\">Ajouter au panier</label>\n";
				}
			}
		} else {
			echo "<h4>Annonce introuvable</h4>";
		}
		echo "</div>\n";
		$nbParPage--;
		$indiceID++;
	}

	if ($estConnecte==1 || $estConnecte==0){
		echo "<p><input type=\"submit\" value=\"Ajouter la sélection à mon panier / Retirer les annonces non sélectionnées du panier\" name=\"panier\"/></p>\n";
	}
	echo "</form>\n";
	echo "<p><a href=\"?q=monpanier\">Accèder à mon panier</a></p>\n";
	echo "<p><a href=\"?e=planifiez\">Accèder à mon planning</a></p>\n";
}	


function panier($bdd,$annonces){
	global $estConnecte;
	global $cookiePanier; //Obliger de modifier les cookie dans une variable temporaire sinon le dernier cookie ecrase les premiers
	if (isset($_POST['panier']) && !isset($_POST[$annonces])){		//retire du panier -> l'annonce n'est plus dans le panier
		if ($estConnecte==1){
			$bdd->exec("DELETE FROM `paniers` WHERE idCompte=$_COOKIE[id_util] and idAnnonce=$annonces");
		} elseif ($estConnecte==0){
			$cook="";
			foreach(explode("/",$cookiePanier) as $value){
				if ($value!=$annonces && $cook==""){
					$cook.=$value;
				} else if ($value!=$annonces) {
					$cook.='/'.$value;
				}
			}
			$cookiePanier=$cook;
		}
		return 0; 
	} else if (isset($_POST['panier']) && isset($_POST[$annonces])){	//ajoute au panier -> l'annonce est dans le panier		
		if ($estConnecte==1){
			$bdd->exec("INSERT INTO `paniers`(`idCompte`, `idAnnonce`) VALUES ('$_COOKIE[id_util]','$annonces')");
		} elseif ($estConnecte==0){			
			if ($cookiePanier==""){
				$cookiePanier=$annonces;
			} else if (!strstr($cookiePanier,$annonces)){
				$cookiePanier.='/'.$annonces;
			}
		}
		return 1;
	} else if (($estConnecte==0 && isset($_COOKIE['panier']) && strstr($_COOKIE['panier'],$annonces)) || ($estConnecte==1 && $bdd->query("SELECT * FROM paniers WHERE idCompte=$_COOKIE[id_util] and idAnnonce=$annonces")->fetch()!=null)) {
		// ne fait rien mais l'annonce est dans le panier
		return 1;
	} else { // ne fait rien mais l'annonce n'est pas dans le panier
		return 0;
	}
}
?>
